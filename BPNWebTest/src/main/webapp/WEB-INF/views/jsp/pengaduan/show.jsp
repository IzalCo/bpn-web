<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<style>
p {
	margin: 0;
	padding: 0;
}

header {
	height: 5em;
}

input {
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
}

input.form-control {
	height: 30px !important;
}

select.form-control {
	height: 30px !important;
}

label {
	display: inline-block;
}

.btnrad {
	border: none;
	outline: none;
	padding: 10px 16px;
	background-color: #a6a6a6;
	cursor: pointer;
}

/* Style the active class (and buttons on mouse-over) */
.active, .btnrad:hover {
	background-color: #262626;
	color: white;
}
</style>

<header>
	<div class="container text-center">
		<h1>Pengaduan</h1>
	</div>
</header>

<body>
	<div id="page-content">
		<div class="container">

			<form id="formulir" class="form-horizontal">

				<div class="form-row text-center justify-content-center mb-1">
					<div class="col-12">PENGADUAN</div>
					<div class="col-2">
						<label for="data.no">NOMOR :</label>
					</div>
					<div class="col-2">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" value="${dat.id}"
								readonly>
						</div>
					</div>
				</div>
				<div class="form-row text-center justify-content-center mb-1">
					<div class="col-2">
						<label for="data.tgl">TANGGAL :</label>
					</div>
					<div class="col-2">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.tgl"
								value="${dat.date}" name="data.tgl" readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-4"></div>

				<div class="form-row mb-1">
					<div class="col-12">
						I. Identitas Pengadu<a class="text-danger mb-3">**)</a>
					</div>
					<div class="col-auto"></div>
					<div class="col-auto">a.</div>
					<div class="col-xl-2 col-sm-3">
						<label for="data.nama">Nama : <a class="text-danger mb-3">*</a></label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.nama"
								value="${dat.name}" name="data.nama" readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-auto"></div>
					<div class="col-auto">b.</div>
					<div class="col-xl-2 col-sm-3">
						<label for="data.alamat">Alamat / alamat email : <a
							class="text-danger mb-3">*</a></label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.alamat"
								value="${dat.alamat}" name="data.alamat" readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-auto"></div>
					<div class="col-auto">c.</div>
					<div class="col-xl-2 col-sm-3">
						<label for="data.job">Pekerjaan : <a
							class="text-danger mb-3">*</a></label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.job"
								value="${dat.pekerjaan}" name="data.job" readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-3">
					<div class="col-auto"></div>
					<div class="col-auto">d.</div>
					<div class="col-xl-2 col-sm-3">
						<label for="data.hp">Nomor Telepon : <a
							class="text-danger mb-3">*</a></label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.hp"
								value="${dat.notelp}" name="data.hp" readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-3">
					<div class="col-12">II. Uraian Pengaduan</div>
					<div class="col-auto">&nbsp;&nbsp;&nbsp;</div>
					<div class="col">
						<textarea class="form-control" id="data.aduan" name="data.aduan"
							rows="7" maxlength="450" readonly>${dat.pengaduan}</textarea>
					</div>
					<div class="col-12"></div>
					<div class="col-auto">&nbsp;&nbsp;&nbsp;&nbsp;</div>
					<div class="col-auto">
						<i>(Diisi dengan keterangan siapa, dimana, mengapa, dengan
							apa, kapan)</i>
					</div>
				</div>

				<div class="form-row mb-5">
					<div class="col-12">III. Bukti Yang Dilampirkan</div>
					<div class="col-auto">&nbsp;&nbsp;&nbsp;&nbsp;</div>
					<div class="col">
						<div class="input-group input-group-sm" id="column-aduan">
							<c:forEach var="buk" items="${dat.bukti}">
								<input type="text" class="form-control" value="${buk}" readonly>
								<div class="col-12 mb-1"></div>
							</c:forEach>
						</div>
					</div>
					<div class="col-12 mb-2"></div>
				</div>

			</form>

		</div>
	</div>
	<jsp:include page="../fragments/footer.jsp" />
	<script>
		if (window.history.replaceState) {
			//window.history.replaceState(null, null, window.location.href);
		}
	</script>
</body>
</html>