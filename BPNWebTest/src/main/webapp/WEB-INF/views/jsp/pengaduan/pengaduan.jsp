<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<style>
p {
	margin: 0;
	padding: 0;
}

header {
	height: 5em;
}

input {
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
}

input.form-control {
	height: 30px !important;
}

select.form-control {
	height: 30px !important;
}

label {
	display: inline-block;
}

.btnrad {
	border: none;
	outline: none;
	padding: 10px 16px;
	background-color: #a6a6a6;
	cursor: pointer;
}

/* Style the active class (and buttons on mouse-over) */
.active, .btnrad:hover {
	background-color: #262626;
	color: white;
}
</style>

<header>
	<div class="container text-center">
		<h1>Formulir Pengaduan</h1>
	</div>
</header>

<body>
	<div id="page-content">
		<div class="container">

			<form id="formulir" class="form-horizontal" method="POST"
				action="/admin/pengaduan">

				<div class="form-row text-center justify-content-center mb-1">
					<div class="col-12">PENGADUAN</div>
					<div class="col-2">
						<label for="data.no">NOMOR :</label>
					</div>
					<div class="col-2">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.no"
								value="${nomor}" name="data.no" readonly>
						</div>
					</div>
				</div>
				<div class="form-row text-center justify-content-center mb-1">
					<div class="col-2">
						<label for="data.tgl">TANGGAL :</label>
					</div>
					<div class="col-2">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.tgl"
								value="${tanggal}" name="data.tgl" readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-4"></div>

				<div class="form-row mb-1">
					<div class="col-12">
						I. Identitas Pengadu<a class="text-danger mb-3">**)</a>
					</div>
					<div class="col-auto"></div>
					<div class="col-auto">a.</div>
					<div class="col-xl-2 col-sm-3">
						<label for="data.nama">Nama : <a class="text-danger mb-3">*</a></label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.nama"
								placeholder="Nama Pengadu" name="data.nama" required>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-auto"></div>
					<div class="col-auto">b.</div>
					<div class="col-xl-2 col-sm-3">
						<label for="data.alamat">Alamat / alamat email : <a
							class="text-danger mb-3">*</a></label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.alamat"
								placeholder="Alamat Pengadu" name="data.alamat" required>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-auto"></div>
					<div class="col-auto">c.</div>
					<div class="col-xl-2 col-sm-3">
						<label for="data.job">Pekerjaan : <a
							class="text-danger mb-3">*</a></label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.job"
								placeholder="Pekerjaan Pengadu" name="data.job" required>
						</div>
					</div>
				</div>

				<div class="form-row mb-3">
					<div class="col-auto"></div>
					<div class="col-auto">d.</div>
					<div class="col-xl-2 col-sm-3">
						<label for="data.hp">Nomor Telepon : <a
							class="text-danger mb-3">*</a></label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.hp"
								placeholder="No. Telepon Pengadu" name="data.hp" required>
						</div>
					</div>
				</div>

				<div class="form-row mb-3">
					<div class="col-12">II. Uraian Pengaduan</div>
					<div class="col-auto">&nbsp;&nbsp;&nbsp;</div>
					<div class="col">
						<textarea class="form-control" id="data.aduan" placeholder="Aduan"
							name="data.aduan" rows="7" maxlength="450" required></textarea>
					</div>
					<div class="col-12"></div>
					<div class="col-auto">&nbsp;&nbsp;&nbsp;&nbsp;</div>
					<div class="col-auto">
						<i>(Diisi dengan keterangan siapa, dimana, mengapa, dengan
							apa, kapan)</i>
					</div>
				</div>

				<div class="form-row mb-5">
					<div class="col-12">III. Bukti Yang Dilampirkan</div>
					<div class="col-auto">&nbsp;&nbsp;&nbsp;&nbsp;</div>
					<div class="col">
						<div class="input-group input-group-sm" id="column-aduan">
							<input type="text" class="form-control" id="data.bukti.1"
								name="data.bukti.1">
							<div class="col-12 mb-1"></div>
						</div>
					</div>
					<div class="col-12 mb-2"></div>
					<div class="col-auto">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
					<button class="btn btn-secondary btn-sm" type="button"
						onClick="addLampiran()">Tambah Bukti</button>
					<p>
						&nbsp;&nbsp;&nbsp;
						<button class="btn btn-danger btn-sm" type="button"
							onClick="remLampiran()">Hapus Bukti</button>
				</div>

				<input type="hidden" name="data.total-bukti" id="data.total-bukti"
					value="1">

				<div class="form-row mb-5">
					<div class="col" align="center">
						<button type="submit" class="btn btn-primary btn-lg">Submit</button>

					</div>
				</div>

			</form>

		</div>
	</div>
	<jsp:include page="../fragments/footer.jsp" />
	<script src="/resources/core/js/form.js"></script>
	<script>
	var count = 1;
	function addLampiran() {
		if(count == 7) {
			window.alert("Cannot add another input!");
			return;
		}
		// Get the lampiran form element
		var lampiran = document.getElementById('column-aduan');

		// Good to do error checking, make sure we managed to get something
		if (lampiran) {

				// Create input
				var newInput = document.createElement('input');
				newInput.type = 'text';
				newInput.name = 'data.bukti.'+ (++count);
				newInput.id = 'data.bukti.'+ (count);
				newInput.className = 'form-control';

				var di = document.createElement('div');
				di.className = 'col-12 mb-1';
				di.id = 'data.space.'+ (count);
				// Good practice to do error checking
				if (newInput && di) {
					// Add the new elements to the form
					lampiran.appendChild(newInput);
					lampiran.appendChild(di);
					document.getElementById('data.total-bukti').value = count;
				}
		}
	}
	
	function remLampiran() {
		if(count == 1) {
			window.alert("Cannot remove another input!");
			return;
		}
		// Get the lampiran form element
		var lampiran = document.getElementById('column-aduan');

		var inp = document.getElementById('data.bukti.'+count);
		var di = document.getElementById('data.space.'+count);
		if(inp && di) {
			lampiran.removeChild(inp);
			lampiran.removeChild(di);
			count--;
			document.getElementById('data.total-bukti').value = count;
		}
	}
	</script>

</body>
</html>