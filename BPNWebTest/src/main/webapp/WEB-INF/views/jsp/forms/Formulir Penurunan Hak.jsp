<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<style>
p {
	margin: 0;
	padding: 0;
}

header {
	height: 7em;
}

input {
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
}

input.form-control {
	height: 30px !important;
}

select.form-control {
	height: 30px !important;
}

label {
	display: inline-block;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
	background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
	display: none;
}

button:hover {
	opacity: 0.8;
}

.btnrad {
	border: none;
	outline: none;
	padding: 10px 16px;
	background-color: #a6a6a6;
	cursor: pointer;
}

/* Style the active class (and buttons on mouse-over) */
.active, .btnrad:hover {
	background-color: #262626;
	color: white;
}

#prevBtn {
	background-color: #bbbbbb;
}

/* Make circles that indicate the steps of the form: */
.step {
	height: 15px;
	width: 15px;
	margin: 0 2px;
	background-color: #bbbbbb;
	border: none;
	border-radius: 50%;
	display: inline-block;
	opacity: 0.5;
}

.step.active {
	background-color: #666666;
	opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
	background-color: #00b33c;
}

/*.progress {
	height: 13px;
	width: 100%;
	margin: 10px 18px;
	background-color: #e6e6e6
}

.progress-bar {
	background-color: #00b300
}*/
</style>

<header>
	<div class="container">
		<h1>Formulir Penurunan Hak</h1>
	</div>
</header>

<body>
	<div id="page-content">
		<div class="container">

			<form id="formulir" class="form-horizontal" method="POST"
				action="/form/ajukan?formName=Formulir%20Penurunan%20Hak">

				<!--<div class="row">
					<div class="progress">
						<div
							class="progress-bar progress-bar-striped progress-bar-animated"
							role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
				</div>-->

				<div class="tab mb-5">
					<p>Dengan hormat,</p>
					<p>Yang bertanda tangan dibawah ini :</p>
					<div class="form-row mb-4"></div>

					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label for="data.nameP">Nama : <a
								class="text-danger mb-3">*</a>
							</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.nameP"
									placeholder="Nama Pemilik Hak" name="data.nameP" required>
							</div>
						</div>
					</div>

					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label for="data.birthP">Tanggal lahir : <a
								class="text-danger mb-3">*</a>
							</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.birthP"
									placeholder="Tanggal Lahir Pemilik Hak" name="data.birthP"
									data-provide="datepicker" data-date-format="dd/mm/yyyy"
									required>
							</div>
						</div>
					</div>

					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label for="data.jobP">Pekerjaan : <a
								class="text-danger mb-3">*</a>
							</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.jobP"
									placeholder="Pekerjaan Pemilik Hak" name="data.jobP" required>
							</div>
						</div>
					</div>

					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label for="data.kewP">Kewarganegaraan : <a
								class="text-danger mb-3">*</a>
							</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<select class="form-control" id="data.kewP" name="data.kewP"
									required>
									<option value="WNI" id="WNI">WNI</option>
									<option value="WNA" id="WNA">WNA</option>
								</select>
							</div>
						</div>
					</div>

					<div class="form-row mb-4">
						<div class="col-xl-2 col-sm-3">
							<label for="data.alamatP">Tempat Tinggal : <a
								class="text-danger mb-3">*</a>
							</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.alamatP"
									placeholder="Alamat Pemilik Hak" name="data.alamatP" required>
							</div>
						</div>
					</div>

					<div class="form-row mb-4">
						<div class="col-auto">
							<p>dalam hal ini bertindak untuk dan</p>
						</div>
						<div class="col">
							<div class="btn-group btn-group-sm btn-group-toggle"
								data-toggle="buttons">

								<label class="btn btn-secondary btn-sm btnrad active"
									data-toggle="collapse" data-target="#kuasa"> <input
									type="radio" name="data.kuasa" id="ya" value="ya"
									autocomplete="off" checked>Atas nama diri sendiri
								</label>

								<p>&nbsp;/&nbsp;</p>

								<label class="btn btn-secondary btm-sm btnrad"
									data-toggle="collapse" data-target="#kuasa"> <input
									type="radio" name="data.kuasa" id="tidak" value="tidak"
									autocomplete="off">Selaku kuasa dari
								</label>

							</div>
							<i class="fa fa-question-circle" aria-hidden="true"
								data-toggle="popover" data-trigger="hover"
								data-placement="right" data-content="Pilih status anda"></i>

						</div>
					</div>

					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label for="data.nameK">Nama :</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.nameK"
									placeholder="Nama Pemegang Kuasa" name="data.nameK" disabled>
							</div>
						</div>
					</div>

					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label for="data.birthK">Tanggal lahir :</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.birthK"
									placeholder="Tanggal Lahir Pemegang Kuasa" name="data.birthK"
									data-provide="datepicker" data-date-format="dd/mm/yyyy"
									disabled>
							</div>
						</div>
					</div>

					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label for="data.jobK">Pekerjaan :</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.jobK"
									placeholder="Pekerjaan Pemegang Kuasa" name="data.jobK"
									disabled>
							</div>
						</div>
					</div>

					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label for="data.kewK">Kewarganegaraan :</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<select class="form-control" id="data.kewK" name="data.kewK"
									disabled>
									<option value="WNI" id="WNI">WNI</option>
									<option value="WNA" id="WNA">WNA</option>
								</select>
							</div>
						</div>
					</div>

					<div class="form-row mb-4">
						<div class="col-xl-2 col-sm-3">
							<label for="data.alamatK">Tempat Tinggal :</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.alamatK"
									placeholder="Alamat Pemegang Kuasa" name="data.alamatK"
									disabled>
							</div>
						</div>
					</div>

					<div class="form-row mb-1">
						<div class="col-xl-auto col-sm-auto">
							<label for="data.noSK">Berdasarkan Surat Kuasa Nomor</label>
						</div>
						<div class="col-xl-2 col-sm-3">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.noSk"
									placeholder="Nomor SK" name="data.noSK" required>
							</div>
						</div>
						<div class="col-xl-auto col-sm-auto">
							<label for="data.tglSK">tanggal</label>
						</div>
						<div class="col-xl-2 col-sm-3">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.tglSK"
									placeholder="Tanggal SK" name="data.tglSk" required>
							</div>
						</div>
						<div class="col-xl-auto col-sm-auto">
							<p>dengan ini mengajukan permohonan pendaftaran
						</div>
					</div>

					<div class="form-row mb-4">
						<div class="col">
							<p>Perubahan Hak Guna Bangunan / Hak Pakai atas bidang tanah
								Hak Guna Bangunan / Hak Milik yang terletak di :
						</div>
					</div>

					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label for="data.desa">Kelurahan :</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<select class="form-control" id="data.desa" name="data.desa"
									required>

								</select>
							</div>
						</div>
					</div>

					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label for="data.kecamatan">Kecamatan :</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<select class="form-control" id="data.kecamatan"
									name="data.kecamatan" required>
									<option value="cilincing">Cilincing</option>
									<option value="kelapa gading">Kelapa Gading</option>
									<option value="koja">Koja</option>
									<option value="pademangan">Pademangan</option>
									<option value="penjaringan">Penjaringan</option>
									<option value="tanjung priok">Tanjung Priok</option>
								</select>
							</div>
						</div>
					</div>

					<div class="form-row mb-4">
						<div class="col-xl-2 col-sm-3">
							<label for="data.kota">Kota Administrasi :</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.kota"
									value="Jakarta Utara" name="data.kota" readonly>
							</div>
						</div>
					</div>

					<div class="form-row mb-4">
						<div class="col">
							<p>berdasarkan Keputusan Menteri Negara Agraria/Kepala Badan
								Pertanahan Nasional Tanggal 9-12-1997 Nomor 16 Tahun 1997
								tentang Perubahan Hak Milik menjadi Hak Guna Bangunan atau Hak
								Milik dan Hak Guna Bangunan menjadi Hak Pakai.
						</div>
					</div>

					<div class="form-row mb-4">
						<div class="col">
							<p>Untuk melengkapi permohonan dimaksud, bersama ini kamu
								lampirkan :
						</div>
					</div>

					<div class="form-row mb-1">

						<div class="col-auto">
							<label>1) Sertipikat Hak Milik/Hak Guna Bangunan No </label>
						</div>

						<div class="col-xl-4 col-sm-5">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.noSerti"
									placeholder="Nomor" name="data.noSerti">
							</div>
						</div>
						<div class="col-auto">
							<p>/</p>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.tahunSerti"
									placeholder="Tahun" name="data.tahunSerti">
							</div>
						</div>

					</div>

					<div class="form-row mb-1">

						<div class="col-auto">
							<label>2) Kutipan Risalah Lelang tanggal </label>
						</div>

						<div class="col-xl-3 col-sm-5">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.tanggalLelang"
									placeholder="Tanggal Risalah Lelang" name="data.tanggalLelang">
							</div>
						</div>
						<div class="col-auto">
							<p>No.</p>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.noLelang"
									placeholder="Nomor Risalah Lelang" name="data.noLelang">
							</div>
						</div>

					</div>

					<div class="form-row mb-1">

						<div class="col-auto">
							<label>3) Surat Keterangan Memasang Hak Tanggungan /
								Surat Persetujuan Bank </label>
						</div>

						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.bank"
									placeholder="" name="data.bank">
							</div>
						</div>

					</div>

					<div class="form-row mb-1">
						<div class="col-auto">
							<p>Tanggal</p>
						</div>

						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.bankTgl"
									placeholder="Tanggal" name="data.bankTgl">
							</div>
						</div>

						<div class="col-auto">
							<p>No.</p>
						</div>

						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.bankNo"
									placeholder="Nomor" name="data.bankNo">
							</div>
						</div>
					</div>

					<div class="form-row mb-1">

						<div class="col-auto">
							<label>4) Bukti identitas pemohon, yaitu : </label>
						</div>

						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.ident"
									placeholder="Lampiran identitas" name="data.ident">
							</div>
						</div>

					</div>

				</div>

				<div class="tab mb-5">

					<div>
						<h3>Surat Pernyataan</h3>
					</div>

					<p>Yang bertanda tangan dibawah ini :</p>
					<div class="form-row mb-4"></div>

					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label>Nama :</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" name="data.nama"
									id="data.nama" readonly>
							</div>
						</div>
					</div>
					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label>Umur</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" name="data.umur"
									id="data.umur" readonly>
							</div>
						</div>
					</div>
					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label>Pekerjaan :</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" name="data.pekerjaan"
									id="data.pekerjaan" readonly>
							</div>
						</div>
					</div>
					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label for="data.ktpP">Nomor KTP : <a
								class="text-danger mb-3">*</a></label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.ktpP"
									placeholder="Nomor KTP Pemilik Hak" name="data.ktpP"
									pattern="[0-9]{16,16}" maxlength="16" required>
							</div>
						</div>
					</div>
					<div class="form-row mb-4">
						<div class="col-xl-2 col-sm-3">
							<label>Alamat :</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" name="data.alamat"
									id="data.alamat" readonly>
							</div>
						</div>
					</div>
					<div class="form-row mb-1">
						<div class="col">
							<p>Dengan ini menyatakan bahwa :
						</div>
					</div>

					<div class="form-row mb-4">
						<div class="col-auto">
							<p>1.
						</div>
						<div class="col">
							<p>Saya pemegang hak atas sebidang tanah yang terletak di :
						</div>
					</div>

					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label for="data.jalanT">Jalan : <a
								class="text-danger mb-3">*</a>
							</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.jalanT"
									placeholder="Nama Jalan" name="data.jalanT" required>
							</div>
						</div>
					</div>

					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label>Desa/Kelurahan :</label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" name="data.desa"
									id="data.desa" readonly>
							</div>
						</div>
					</div>

					<div class="form-row mb-1">
						<div class="col-xl-2 col-sm-3">
							<label for="data.luasT">Luas Tanah : <a
								class="text-danger mb-3">*</a>
							</label>
						</div>
						<div class="col-xl-6 col-sm-8">
							<div class="input-group input-group-sm">
								<input type="number" class="form-control" id="data.luasT"
									placeholder="Luas" name="data.luasT" required>
							</div>
						</div>
						<div class="col">
							<p>M&sup2;</p>
						</div>
					</div>

					<div class="form-row mb-4">
						<div class="col-xl-2 col-sm-3">
							<label for="data.statusT">Status Tanah : <a
								class="text-danger mb-3">*</a>
							</label>
						</div>
						<div class="col-xl-4 col-sm-6">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.statusT"
									placeholder="Status Tanah" name="data.statusT" required>
							</div>
						</div>
						<div class="col-auto">
							<label for="data.noT">Nomor </label>
						</div>
						<div class="col">
							<div class="input-group input-group-sm">
								<input type="text" class="form-control" id="data.noT"
									placeholder="Nomor Tanah" name="data.noT" required>
							</div>
						</div>
					</div>

					<div class="form-row mb-4">
						<div class="col-auto">
							<p>2.
						</div>
						<div class="col">
							<p>
								Bahwa sampai saat ini kami kuasai secara fisik, tidak
								dijadikan/menjadikan jaminan piutang atau tidak dibebani dari
								beban-beban lainnya, tidak dalam keadaan sengketa dan tidak
								dikenakan suatu sitaan dan <b>belum pernah dilakukan
									pengikatan jual beli ataupun diperjual belikan kepada pihak
									lain.</b>
						</div>
					</div>

					<div class="form-row mb-4">
						<div class="col-auto">
							<p>3.
						</div>
						<div class="col">
							<p>Bahwa Hak Milik dimaksud kami ajukan permohonan Penurunan
								Haknya menjadi Hak Guna Bangunan dikarenakan bidang tanahnya
								akan kami jual kepada Badan Hukum setelah selesai proses
								penurunan haknya dan segala akibat dari penurunan haknya menjadi
								tanggung jawab kami sebagai pemegang hak / pemilik.
						</div>
					</div>

					<div class="form-row mb-4">
						<div class="col-auto">
							<p>4.
						</div>
						<div class="col">
							<p>Surat pernyataan ini saya buat dengan sebenarnya dengan
								penuh tanggung jawab dan apabila bukti-bukti yang saya
								ajukan/lampirkan dalam permohonan di Kantor Pertanahan Kota
								Administrasi Jakarta Utara, ternyata dikemudian hari dinyatakan
								tidak benar dan atau apabila dikernudian han terdapat tuntutan
								pidana atau gugatan perdata atau gugatan Tata Usaha Negara maka
								saya akan bertanggung jawab sepenuhnya untuk menyelesaikan
								tuntutan pidana atau gugatan perdata atau gugatan Tata Usaha
								Negara tersebut tanpa melibatkan Badan Pertanahan Nasional cq
								Kantor Pertanahan Kota Administrasi Jakarta Utara.
						</div>
					</div>

					<div class="form-row mb-4">
						<div class="col-auto">
							<p>5.
						</div>
						<div class="col">
							<p>Apabila dikemudian hari ternyata Surat Pernyataan ini
								tidak benar, maka saya dianggap memberikan keterangan palsu pada
								Pemerintah sesuai pasal 242 (ayat 1, 2 dan 3), 263, 266, 363,
								372, dan 378 KUH Pidana dan oleh sebab itu saya bertanggung
								jawab serta bersedia ditindak sesuai dengan ketentuan
								perundang-undangan yang berlaku.
						</div>
					</div>

					<div class="form-row mb-4">
						<div class="col">
							<p>
								Demikian Surat Pernyataan ini saya buat dalam rangka kelengkapan
								permohonan <b>Penurunan Hak</b> di Kantor Pertanahan Kota
								Administrasi Jakarta Utara.
						</div>
					</div>


				</div>

				<div style="overflow: auto;">
					<div style="float: right;">
						<button type="button" class="btn btn-primary" id="prevBtn"
							onclick="nextPrev(-1);autoFill(-1);">Previous</button>
						<button type="button" class="btn btn-info" id="nextBtn"
							onclick="nextPrev(1);autoFill(1);">Next</button>
					</div>
				</div>

				<!-- Circles which indicates the steps of the form: -->
				<div id="indicator" class="mb-5"
					style="text-align: center; margin-top: 40px;"></div>

			</form>

		</div>
	</div>

	<script src="/resources/core/js/form.js"></script>
	<script src="/resources/core/js/keckel.js"></script>
	<script src="/resources/core/js/steps.js"></script>
	<script>
		$('[data-toggle="popover"]').popover();
	</script>
	<script>
		var useKuasa = false;

		document.getElementById('ya').onchange = function() {
			document.getElementById('data.nameK').disabled = true;
			/*document.getElementById('data.nameK').value = "";*/

			document.getElementById('data.birthK').disabled = true;
			/*document.getElementById('data.umurK').value = "";*/

			document.getElementById('data.jobK').disabled = true;
			/*document.getElementById('data.jobK').value = "";*/

			document.getElementById('data.kewK').disabled = true;
			/*document.getElementById('data.ktpK').value = "";*/

			document.getElementById('data.alamatK').disabled = true;
			/*document.getElementById('data.alamatK').value = "";*/
			useKuasa = false;
		}

		document.getElementById('tidak').onchange = function() {
			document.getElementById('data.nameK').disabled = false;

			document.getElementById('data.birthK').disabled = false;

			document.getElementById('data.jobK').disabled = false;

			document.getElementById('data.kewK').disabled = false;

			document.getElementById('data.alamatK').disabled = false;

			useKuasa = true;
		}

		var ct = 0;
		function autoFill(n) {
			ct += n;
			if (ct == 1) {
				var nama = document.getElementById(useKuasa ? 'data.nameK'
						: 'data.nameP').value;
				document.getElementById('data.nama').value = nama;

				var d = document.getElementById(useKuasa ? 'data.birthK'
						: 'data.birthP').value;
				d = [ d.slice(-4), d.slice(3, 5), d.slice(0, 2) ].join('/');
				document.getElementById('data.umur').value = calcAge(d);

				var j = document.getElementById(useKuasa ? 'data.jobK'
						: 'data.jobP').value;
				document.getElementById('data.pekerjaan').value = j;

				var a = document.getElementById(useKuasa ? 'data.alamatK'
						: 'data.alamatP').value;
				document.getElementById('data.alamat').value = a;

				var e = document.getElementById('data.desa');
				var des = e.options[e.selectedIndex].text;
				document.getElementById('data.desa').value = des;
			}
		}

		function calcAge(dateString) {
			var birthday = +new Date(dateString);
			return ~~((Date.now() - birthday) / (31557600000));
		}
	</script>
	<jsp:include page="../fragments/footer.jsp" />
</body>
</html>