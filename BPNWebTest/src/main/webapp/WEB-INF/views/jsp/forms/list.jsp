<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<body class="d-flex flex-column">
	<div id="page-content">
		<div class="container">

			<h1>Formulir Online</h1>

			<table class="table table-striped">
				<thead class="">
					<tr>
						<th>Jenis Pendaftaran</th>
						<th>Aksi</th>
						<th>Bantuan</th>
					</tr>
				</thead>

				<tbody>
					<c:forEach var="form" items="${forms}" varStatus="fs">
						<tr>
							<td>${form}</td>
							<td>
								<button class="btn btn-info"
									onclick="location.href='/form/ajukan?formName=${form}'">Isi
									Form</button>
								<button class="btn btn-primary" data-toggle="modal"
									data-target="#modal-${fs.index}">Lihat Form</button>

								<div class="modal fade" id="modal-${fs.index}" role="dialog">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title text-center" id="myModalLabel">${form}</h4>
											</div>
											<div class="modal-body">
												<div style="text-align: center;">
													<iframe src="/form?formName=${form}"
														style="width: 100%; height: 500px;"></iframe>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default"
													data-dismiss="modal">Tutup</button>
												<button type="button" class="btn btn-primary"
													onclick="location.href='/form/ajukan?formName=${form}'">Isi
													Form</button>
											</div>
										</div>
									</div>
								</div>

							</td>
							<td>
								<button class="btn btn-light" data-toggle="modal"
									data-target="#help-${fs.index}">
									<i class="fa fa-question-circle" aria-hidden="true"></i>
								</button>

								<div class="modal fade" id="help-${fs.index}" role="dialog">
									<div class="modal-dialog modal-confirm">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title">Petunjuk Pengisian</h4>
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true">&times;</button>
											</div>
											<div class="modal-body">
												<p>{help}</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-info"
													data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>

	<jsp:include page="../fragments/footer.jsp" />

</body>
</html>