<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<style>
p {
	margin: 0;
	padding: 0;
}

header {
	height: 5em;
}

input {
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
}

input.form-control {
	height: 30px !important;
}

select.form-control {
	height: 30px !important;
}

label {
	display: inline-block;
}

.btnrad {
	border: none;
	outline: none;
	padding: 10px 16px;
	background-color: #a6a6a6;
	cursor: pointer;
}

/* Style the active class (and buttons on mouse-over) */
.active, .btnrad:hover {
	background-color: #262626;
	color: white;
}
</style>

<header>
	<div class="container">
		<h1>Lampiran13</h1>
	</div>
</header>

<body>
	<div id="page-content">
		<div class="container">

			<form id="formulir" class="form-horizontal" method="POST"
				action="/form/ajukan?formName=Lampiran%2013">

				<p>Dengan hormat,</p>
				<p>Yang bertanda tangan dibawah ini :</p>
				<div class="form-row mb-4"></div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label for="data.nameP">Nama : <a class="text-danger mb-3">*</a></label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.nameP"
								placeholder="Nama Pemilik Hak" name="data.nameP" required>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label for="data.umurP">Umur : <a class="text-danger mb-3">*</a></label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="number" class="form-control" id="data.umurP"
								placeholder="Umur Pemilik Hak" name="data.umurP" required>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label for="data.jobP">Pekerjaan : <a
							class="text-danger mb-3">*</a></label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.jobP"
								placeholder="Pekerjaan Pemilik Hak" name="data.jobP" required>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label for="data.ktpP">Nomor KTP : <a
							class="text-danger mb-3">*</a></label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.ktpP"
								placeholder="Nomor KTP Pemilik Hak" name="data.ktpP"
								pattern="[0-9]{16,16}" maxlength="16" required>
						</div>
					</div>
				</div>

				<div class="form-row mb-4">
					<div class="col-xl-2 col-sm-3">
						<label for="data.alamatP">Alamat : <a
							class="text-danger mb-3">*</a></label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.alamatP"
								placeholder="Alamat Pemilik Hak" name="data.alamatP" required>
						</div>
					</div>
				</div>

				<div class="form-row mb-4">
					<div class="col-xl-2 col-sm-3">
						<p>Dalam hal ini untuk dan &nbsp;</p>
					</div>
					<div class="col">
						<div class="btn-group btn-group-sm btn-group-toggle"
							data-toggle="buttons">

							<label class="btn btn-secondary btn-sm btnrad active"
								data-toggle="collapse" data-target="#kuasa"> <input
								type="radio" name="data.kuasa" id="ya" value="ya"
								autocomplete="off" checked> Atas nama diri sendiri
							</label>

							<p>&nbsp;/&nbsp;</p>

							<label class="btn btn-secondary btm-sm btnrad"
								data-toggle="collapse" data-target="#kuasa"> <input
								type="radio" name="data.kuasa" id="tidak" value="tidak"
								autocomplete="off"> Selaku kuasa dari
							</label>
						</div>
						<i class="fa fa-question-circle" aria-hidden="true"
							data-toggle="popover" data-trigger="hover" data-placement="right"
							data-content="Pilih status anda"></i>
					</div>
				</div>


				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label for="data.nameK">Nama :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.nameK"
								placeholder="Nama Pemegang Kuasa" name="data.nameK" disabled>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label for="data.umurK">Umur :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="number" class="form-control" id="data.umurK"
								placeholder="Umur Pemegang Kuasa" name="data.umurK" disabled>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label for="data.jobK">Pekerjaan :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.jobK"
								placeholder="Kekerjaan Pemegang Kuasa" name="data.jobK" disabled>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label for="data.ktpK">Nomor KTP :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.ktpK"
								placeholder="Nomor KTP Pemegang Kuasa" name="data.ktpK"
								pattern="[0-9]{16,16}" maxlength="16" disabled>
						</div>
					</div>
				</div>

				<div class="form-row mb-4">
					<div class="col-xl-2 col-sm-3">
						<label for="data.alamatK">Alamat :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.alamatK"
								placeholder="Alamat Pemegang Kuasa" name="data.alamatK" disabled>
						</div>
					</div>
				</div>


				<div class="form-row mb-1">
					<div class="col-xl-auto col-sm-4">
						<label for="data.noSK">Berdasarkan Surat Kuasa Nomor</label>
					</div>
					<div class="col-xl-2 col-sm-3">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.noSk"
								placeholder="Nomor SK" name="data.noSK" required>
						</div>
					</div>
					<div class="col-xl-auto col-sm-3">
						<label for="data.tglSK">tanggal</label>
					</div>
					<div class="col-xl-2 col-sm-3">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.tglSK"
								placeholder="Tanggal SK" name="data.tglSk" required>
						</div>
					</div>
					<div class="col-xl-auto col-sm-4">
						<p>dengan ini mengajukan permohonan :
					</div>
				</div>

				<div class="form-row mb-3"></div>

				<div class="form-row mb-3">
					<div class="col-xl-5 col-sm-5">
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="1" value="1"
								checked>Pengukuran
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="2" value="2">Konversi/Pendaftaran
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="3" value="3">Pendaftaran
								Hak Milik Sarusun
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="4" value="4">Pendaftaran
								Tanah Wakaf
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="5" value="5">Pendaftaran
								Peralihan
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="6" value="6">Pendaftaran
								Pemindahan Hak
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="7" value="7">Pendaftaran
								Perubahan Hak
							</label>
						</div>
					</div>

					<div class="col-xl-7 col-sm-7">
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="8" value="8">Pemecahan/Penggabungan
								Hak
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="9" value="9">Pendaftaran
								Hak Tanggungan
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="10"
								value="10">Roya Atas Hak Tanggungan
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="11"
								value="11">Penerbitan Hak Pengganti
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="12"
								value="12">Surat Keterangan Pendaftaran Tanah
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="13"
								value="13">Pengecekan Sertifikat
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="14"
								value="14">Pencatatan <input type="text"
								id="data.pencatatan"
								placeholder="..................................................................."
								name="data.pencatatan">
							</label>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">

					<div class="col-12 mb-3">
						<p>Atas bidang Tanah Hak/Tanah Negara :
					</div>

					<div class="col-xl-2 col-sm-3">
						<label for="data.letakTanah">Terletak di :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.letakTanah"
								placeholder="" name="data.letakTanah" required>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label for="data.desa">Desa/Kelurahan :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<select class="form-control" id="data.desa" name="data.desa"
								required>

							</select>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label for="data.kecamatan">Kecamatan :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<select class="form-control" id="data.kecamatan"
								name="data.kecamatan" required>
								<option value="cilincing">Cilincing</option>
								<option value="kelapa gading">Kelapa Gading</option>
								<option value="koja">Koja</option>
								<option value="pademangan">Pademangan</option>
								<option value="penjaringan">Penjaringan</option>
								<option value="tanjung priok">Tanjung Priok</option>
							</select>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label for="data.kota">Kabupaten/Kotamadya :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.kota"
								value="Jakarta Utara" name="data.kota" readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label for="data.noHak">Nomor Hak :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.noHak"
								placeholder="" name="data.noHak" required>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-12 col-sm-12">
						<label>Untuk melengkapi permohonan dimaksud bersama ini
							kami lampirkan :</label>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-12 col-sm-12">

						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.lampiran1"
								placeholder="" name="data.lampiran1">
						</div>

					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-12 col-sm-12">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.lampiran2"
								placeholder="" name="data.lampiran2">
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-12 col-sm-12">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.lampiran3"
								placeholder="" name="data.lampiran3">
						</div>
					</div>
				</div>

				<div class="form-row mb-5">
					<div class="col-xl-12 col-sm-12">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="data.lampiran4"
								placeholder="" name="data.lampiran4">
						</div>
					</div>
				</div>

				<div class="form-row mb-5">
					<div class="col" align="center">
						<button type="submit" class="btn btn-primary btn-lg">Submit</button>

					</div>
				</div>

			</form>

		</div>
	</div>
	<jsp:include page="../fragments/footer.jsp" />
	<script src="/resources/core/js/form.js"></script>
	<script src="/resources/core/js/keckel.js"></script>
	<script>
		$('[data-toggle="popover"]').popover();
	</script>
	<script>
		document.getElementById('ya').onchange = function() {
			document.getElementById('data.nameK').disabled = true;
			/*document.getElementById('data.nameK').value = "";*/

			document.getElementById('data.umurK').disabled = true;
			/*document.getElementById('data.umurK').value = "";*/

			document.getElementById('data.jobK').disabled = true;
			/*document.getElementById('data.jobK').value = "";*/

			document.getElementById('data.ktpK').disabled = true;
			/*document.getElementById('data.ktpK').value = "";*/

			document.getElementById('data.alamatK').disabled = true;
			/*document.getElementById('data.alamatK').value = "";*/

		}

		document.getElementById('tidak').onchange = function() {
			document.getElementById('data.nameK').disabled = false;

			document.getElementById('data.umurK').disabled = false;

			document.getElementById('data.jobK').disabled = false;

			document.getElementById('data.ktpK').disabled = false;

			document.getElementById('data.alamatK').disabled = false;
		}
	</script>

</body>
</html>