<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />
<script src="/resources/core/js/search.js"></script>

<style>
.form-control:focus {
	border-color: #25476a;
	outline: 0;
	-webkit-box-shadow: none;
	box-shadow: none;
}

#trs-hd {
	text-align: center;
}

.counter {
	padding: 8px;
	color: #ccc;
}

.results tr[visible='false'], .no-result {
	display: none;
}

.results tr[visible='false'], .no-result {
	display: none;
}

.counter {
	padding: 8px;
	color: #ccc;
}

.fa.fa-warning {
	font-size: 18px;
	color: #ff0000;
}

.warning.no-result {
	text-align: center;
}

.search.form-control {
	border-radius: 0px !important;
}

.search-table-col {
	margin-top: 100px;
}
</style>

<body>
	<div id="page-content">
		<div class="container-fluid">

			<div class="card shadow">
				<div class="card-header py-3">
					<p class="text-primary m-0 font-weight-bold">Users</p>
				</div>

				<div class="card-body">

					<div class="col-md-12 search-table-col">

						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<input type="text" class="search form-control"
										placeholder="Search by typing here..">
								</div>
							</div>
						</div>

						<div class="table-responsive search-table-col mt-2 results"
							id="dataTable" role="grid" aria-describedby="dataTable_info">
							<table class="table table-hover dataTable my-0" id="dataTable">
								<thead>
									<tr>
										<th>Username</th>
										<th>Full Name</th>
										<th>Processed Report</th>
									</tr>
								</thead>
								<tbody>

									<tr class="warning no-result">
										<td colspan="12"><i class="fa fa-warning"></i>&nbsp; No
											Result!</td>
									</tr>

									<c:forEach var="user" items="${users}">
										<tr>
											<td>${user.username}</td>
											<td>${user.fullName}</td>
											<td>1</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>

	<jsp:include page="../fragments/footer.jsp" />

</body>
</html>