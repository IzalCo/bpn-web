<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html>

<jsp:include page="../fragments/header.jsp" />

<body>

	<header class="bounce animated masthead text-white text-center"
		style="background: url('/resources/img/bg-masthead.jpg') no-repeat center center; background-size: cover;">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-xl-9 mx-auto">
					<h1 class="mb-5">BPN Jakarta Utara</h1>
				</div>
			</div>
		</div>
	</header>

	<section class="features-icons bg-light text-center">
		<div class="container">
			<div class="row pulse animated">

				<div class="col-lg-4" onclick="location.href='formlist';"
					style="cursor: pointer;">
					<div class="mx-auto features-icons-item mb-5 mb-lg-0 mb-lg-3">
						<div class="d-flex features-icons-icon">
							<i class="icon-doc m-auto text-primary"
								data-bs-hover-animate="pulse"></i>
						</div>
						<h3>Pengajuan Formulir Online</h3>
						<p class="lead mb-0">Mudahnya mengajukan permohonan tanpa
							harus mengambil formulir!</p>
					</div>
				</div>

				<div class="col-lg-4"
					onclick="location.href='https://www.lapor.go.id/instansi/kantor-pertanahan-jakarta-utara';"
					style="cursor: pointer;">
					<div class="mx-auto features-icons-item mb-5 mb-lg-0 mb-lg-3">
						<div class="d-flex features-icons-icon">
							<i class="icon-question m-auto text-primary"
								data-bs-hover-animate="pulse"></i>
						</div>
						<h3>Punya pertanyaan?</h3>
						<p class="lead mb-0">Klik disini untuk mengajukan pertanyaan
							mengenai warkah, dan lain sebagainya</p>
					</div>
				</div>

				<div class="col-lg-4"
					onclick="location.href='https://wa.me/628111245353';"
					style="cursor: pointer;">
					<div class="mx-auto features-icons-item mb-5 mb-lg-0 mb-lg-3">
						<div class="d-flex features-icons-icon">
							<i class="fa fa-whatsapp m-auto text-primary"
								data-bs-hover-animate="pulse"></i>
						</div>
						<h3>Mau lebih dekat?</h3>
						<p class="lead mb-0">Konsultasi dengan admin kami secara real
							time dengan Whatsapp</p>
					</div>
				</div>

			</div>
		</div>
	</section>

	<section class="showcase">
		<div class="container-fluid p-0">
			<div class="row no-gutters">
				<div class="col-lg-6 order-lg-2 text-white showcase-img"
					style="background-image: url(/resources/img/visi.png); background-repeat: no-repeat; background-size: auto; background-position: center;">
					<span></span>
				</div>
				<div class="col-lg-6 my-auto order-lg-1 showcase-text">
					<h2>VISI</h2>
					<p class="lead mb-0">Menjadi lembaga yang mampu mewujudkan
						tanah dan pertanahan untuk sebesar-besar kemakmuran rakyat, serta
						keadilan dan keberlanjutan sistem kemasyarakatan, kebangsaan dan
						kenegaraan Republik Indonesia.</p>
				</div>
			</div>
			<div class="row no-gutters">
				<div class="col-lg-6 text-white showcase-img"
					style="background-image: url(/resources/img/misi.png); background-repeat: no-repeat; background-size: auto; background-position: center;">
					<span></span>
				</div>
				<div class="col-lg-6 my-auto order-lg-1 showcase-text">
					<h2>MISI</h2>
					<p class="lead mb-0">
						Mengembangkan dan menyelenggarakan politik dan kebijakan
						pertanahan untuk: <br> <br>1. Peningkatan kesejahteraan
						rakyat, penciptaan sumber-sumber baru kemakmuran rakyat,
						pengurangan kemiskinan dan kesenjangan pendapatan, serta
						pemantapan ketahanan pangan. <br> <br>2. peningkatan
						tatanan kehidupan bersama yang lebih berkeadilan dan bermartabat
						dalam kaitannya dengan penguasaan, pemilikan, penggunaan dan
						pemanfaatan tanah (P4T). <br> <br>3. Perwujudan tatanan
						kehidupan bersama yang harmonis dengan mengatasi berbagai
						sengketa, konflik dan perkara pertanahan di seluruh tanah air dan
						penataan perangkat hukum dan sistem pengelolaan pertanahan
						sehingga tidak melahirkan sengketa, konflik dan perkara di
						kemudian hari. <br> <br>4. Keberlanjutan sistem
						kemasyarakatan, kebangsaa dan kenegaraan Indonesia dengan
						memberikan akses seluas-luasnya pada generasi yang akan datang
						terhadap tanah sebagai sumber kesejahteraan masyarakat. Menguatkan
						lembaga pertanahan sesuai dengan jiwa, semangat, prinsip dan
						aturan yang tertuang dalam UUPA dan aspirasi rakyat secara luas.
					</p>
				</div>
			</div>

			<div class="row no-gutters">
				<div class="col-lg-6 order-lg-2 text-white">
					<iframe width="100%" height="480px"
						src="https://www.youtube.com/embed/WYyH6jrgcTQ"></iframe>
					<span></span>
				</div>
				<div class="col-lg-6 my-auto order-lg-1 showcase-text">
					<h2>Pelayanan Digital Online</h2>
					<p class="lead mb-0">Kantor Pertanahan Kota Administrasi
						Jakarta Utara Jawab Tantangan Revolusi Industri 4.0</p>
				</div>
			</div>

		</div>
	</section>

	<section class="testimonials text-center bg-light">

		<div class="container">
			<h2 class="mb-5">Pejabat</h2>
			<div class="row">

				<div class="col-lg-12">
					<div class="mx-auto testimonial-item mb-5 mb-lg-0">
						<img class="rounded-circle img-fluid mb-3"
							src="/resources/img/pejabat1.jpeg">
						<h5>Drs. Hiskia Simarmata, M.Si.</h5>
						<p class="font-weight-light mb-0">Kepala Kantor</p>
					</div>
				</div>

			</div>
		</div>

	</section>

	<section class="call-to-action text-white text-center"
		style="background: url(/resources/img/bg-masthead.jpg) no-repeat center center; background-size: cover;">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-xl-9 mx-auto">
					<h2 class="mb-4">BPN Jakarta Utara</h2>
				</div>
			</div>
		</div>
	</section>

	<jsp:include page="../fragments/footer.jsp" />

</body>

</html>