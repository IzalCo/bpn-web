<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />
<style>
.block_content1 h2 {
	font-size: 20px;
}

.block_content h2 {
	font-size: 20px;
}

.dashboard-widget {
	background: #f6f6f6;
	border-top: 5px solid #79C3DF;
	border-radius: 3px;
	padding: 5px 10px 10px;
}

.dashboard-widget .dashboard-widget-title {
	font-weight: 400;
	border-bottom: 1px solid #c1cdcd;
	margin: 0 0 10px;
	padding-bottom: 5px;
	padding-left: 40px;
	line-height: 30px;
}

.dashboard-widget .dashboard-widget-title i {
	font-size: 100%;
	margin-left: -35px;
	margin-right: 10px;
	color: #33a1c9;
	padding: 3px 6px;
	border: 1px solid #abd9ea;
	border-radius: 5px;
	background: #fff;
}

ul.timeline li {
	position: relative;
	border-bottom: 1px solid #e8e8e8;
	clear: both;
}

.timeline .block {
	margin: 0 0 0 105px;
	border-left: 3px solid #e8e8e8;
	overflow: visible;
	padding: 10px 15px;
}

.timeline.widget {
	min-width: 0;
	max-width: inherit;
}

.timeline.widget .block {
	margin-left: 5px;
}

.timeline .tags {
	position: absolute;
	top: 15px;
	left: 0;
	width: 84px;
}

.timeline .tag {
	display: block;
	height: 30px;
	font-size: 13px;
	padding: 8px;
}

.timeline .tag span {
	display: block;
	overflow: hidden;
	width: 100%;
	white-space: nowrap;
	text-overflow: ellipsis;
}

.tag {
	line-height: 1;
	background: #1ABB9C;
	color: #fff !important;
}

.tag:after {
	content: " ";
	height: 30px;
	width: 0;
	position: absolute;
	left: 100%;
	top: 0;
	margin: 0;
	pointer-events: none;
	border-top: 14px solid transparent;
	border-bottom: 14px solid transparent;
	border-left: 11px solid #1ABB9C;
}

.timeline h2.title {
	position: relative;
	font-size: 16px;
	margin: 0;
}

.timeline h2.title:before {
	content: "";
	position: absolute;
	left: -23px;
	top: 3px;
	display: block;
	width: 14px;
	height: 14px;
	border: 3px solid #d2d3d2;
	border-radius: 14px;
	background: #f9f9f9;
}

.timeline .byline {
	padding: .25em 0;
}

.byline {
	font-size: .9375em;
	line-height: 1.3;
	color: #aab6aa;
}

ul.timeline li {
	border: none;
	line-height: 1.8;
}

.timeline.widget .block {
	border-bottom: 1px solid #e8e8e8;
}

.block_content1 ul li {
	margin: 20px;
}
</style>
<body>
	<div class="container">
		<div id="page-content">
			<div class="row justify-content-center mt-0">
				<div class="col-xl-4 col-sm-12 p-0 mt-3 mb-2">
					<div class="card px-0 pt-4 pb-0 mt-0 mb-3"></div>
				</div>
				<div class="col-xl-8 col-sm-12">
					<div class="card px-0 pt-4 pb-0 mt-0 mb-3">${data}</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../fragments/footer.jsp" />
	<script src="/resources/core/js/test.js"></script>
</body>
</html>