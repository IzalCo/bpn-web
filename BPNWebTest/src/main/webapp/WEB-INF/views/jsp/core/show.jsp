<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<style>
header {
	height: 5em;
}

.fit-image {
	width: 100%;
	object-fit: cover
}

p.bordersss {
	border-left: 5px solid green;
	background-color: lightgreen;
}
</style>

<header>
	<div class="container">
		<h1>Hasil Form</h1>
	</div>
</header>

<div class="container">
	<div id="page-content">
		<div class="row justify-content-center mt-0">
			<div class="col-xl-4 col-sm-12 text-center p-0 mt-3 mb-2">
				<div class="card px-0 pt-4 pb-0 mt-0 mb-3">
					<div class="swal2-icon swal2-success swal2-animate-success-icon"
						style="display: flex;">
						<div class="swal2-success-circular-line-left"
							style="background-color: rgb(255, 255, 255);"></div>
						<span class="swal2-success-line-tip"></span> <span
							class="swal2-success-line-long"></span>
						<div class="swal2-success-ring"></div>
						<div class="swal2-success-fix"
							style="background-color: rgb(255, 255, 255);"></div>
						<div class="swal2-success-circular-line-right"
							style="background-color: rgb(255, 255, 255);"></div>
					</div>
					<p>Harap simpan barcode dan print formulir!</p>
					<img alt="img" src="data:image/png;base64,${qrCode}"
						class="fit-image"></img>
				</div>
			</div>
			<div class="col-xl-8 col-sm-12">
				<iframe src="data:application/pdf;base64,${formFile}"
					style="width: 100%; height: 1100px;"></iframe>
			</div>
		</div>

	</div>
</div>

<jsp:include page="../fragments/footer.jsp" />
<script>
	if (window.history.replaceState) {
		window.history.replaceState(null, null, window.location.href);
	}
</script>

</body>
</html>