<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html>

<jsp:include page="../fragments/header.jsp" />

<head>
<link rel="stylesheet" href="/resources/core/css/styles.min.css">
</head>

<body>
	<div class="login-dark"
		style="background-image: url(/resources/img/bg-masthead.jpg);">
		<form method="post">
			<h2 class="sr-only">Login Form</h2>
			<div class="illustration">
				<i class="icon ion-ios-locked-outline"></i>
			</div>
			<div class="form-group ${error != null ? 'has-error' : ''}">
				<span>${message}</span>
				<div class="form-group">
					<input class="form-control" type="text" name="username"
						id="username" placeholder="Username" autofocus>
				</div>
				<div class="form-group">
					<input class="form-control" type="password" name="password"
						id="password" placeholder="Password">
					<form:errors path="password"></form:errors>
				</div>
				<div class="form-group">
					<button class="btn btn-primary btn-block" type="submit">Log
						In</button>
				</div>
			</div>
		</form>
	</div>
	<jsp:include page="../fragments/footer.jsp" />
</body>

</html>