<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />
<script src="/resources/core/js/jsQR.js"></script>
<style>
#loadingMessage {
	text-align: center;
	padding: 40px;
	background-color: #eee;
}

#canvas {
	width: 100%;
}
</style>

<body>
	<div class="container">
		<div id="page-content">
			<div class="row justify-content-center mt-0">
				<div class="col-xl-8 col-sm-12 text-center p-0 mt-3 mb-2">
					<div class="card px-0 pt-4 pb-0 mt-0 mb-3">
						<h1>Form Reader</h1>
						<p>Scan barcode disini</p>
						<div id="loadingMessage">Tidak dapat mengakses kamera!</div>
						<canvas id="canvas"></canvas>
						<button class="btn btn-primary btn-lg" onClick="toggleCamera()">Buka
							/ Tutup Kamera</button>
					</div>
				</div>
			</div>

		</div>
	</div>

	<jsp:include page="../fragments/footer.jsp" />
	<script>
		var video = document.createElement("video");
		var canvasElement = document.getElementById("canvas");
		var canvas = canvasElement.getContext("2d");
		var loadingMessage = document.getElementById("loadingMessage");

		var state = false;

		function toggleCamera() {
			if (!state) {
				vidOn();
				state = true;
			} else {
				vidOff();
				state = false;
			}
		}

		function drawLine(begin, end, color) {
			canvas.beginPath();
			canvas.moveTo(begin.x, begin.y);
			canvas.lineTo(end.x, end.y);
			canvas.lineWidth = 4;
			canvas.strokeStyle = color;
			canvas.stroke();
		}

		function vidOn() {
			// Use facingMode: environment to attemt to get the front camera on phones
			navigator.mediaDevices.getUserMedia({
				video : {
					facingMode : "environment"
				}
			}).then(function(stream) {
				video.srcObject = stream;
				video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
				video.play();
				requestAnimationFrame(tick);
			});
		}

		function tick() {
			loadingMessage.innerText = "Loading video..."
			if (video.readyState === video.HAVE_ENOUGH_DATA) {
				loadingMessage.hidden = true;

				canvasElement.height = video.videoHeight;
				canvasElement.width = video.videoWidth;
				canvas.drawImage(video, 0, 0, canvasElement.width,
						canvasElement.height);
				var imageData = canvas.getImageData(0, 0, canvasElement.width,
						canvasElement.height);
				var code = jsQR(imageData.data, imageData.width,
						imageData.height, {
							inversionAttempts : "dontInvert",
						});
				if (code) {
					drawLine(code.location.topLeftCorner,
							code.location.topRightCorner, "#FF3B58");
					drawLine(code.location.topRightCorner,
							code.location.bottomRightCorner, "#FF3B58");
					drawLine(code.location.bottomRightCorner,
							code.location.bottomLeftCorner, "#FF3B58");
					drawLine(code.location.bottomLeftCorner,
							code.location.topLeftCorner, "#FF3B58");
					window.open("/admin/form/fetch?formUUID=" + code.data);
				}
			}
			requestAnimationFrame(tick);
		}

		function vidOff() {
			//clearInterval(theDrawLoop);
			//ExtensionData.vidStatus = 'off';
			video.srcObject.getTracks()[0].stop();
			video.pause();
			video.src = "";
		}
	</script>

</body>
</html>