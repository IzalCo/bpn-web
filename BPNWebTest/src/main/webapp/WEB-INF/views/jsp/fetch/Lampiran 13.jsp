<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<style>
p {
	margin: 0;
	padding: 0;
}

header {
	height: 5em;
}

input {
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
}

input.form-control {
	height: 30px !important;
}

select.form-control {
	height: 30px !important;
}

label {
	display: inline-block;
}
</style>

<header>
	<div class="container">
		<h1>Lampiran13</h1>
		<small>(UUID: '${uuid}' )</small>
	</div>
</header>

<body>
	<div id="page-content">
		<div class="container">

			<form id="formulir" class="form-horizontal">

				<p>Dengan hormat,</p>
				<p>Yang bertanda tangan dibawah ini :</p>
				<div class="form-row mb-4"></div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label>Nama :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" value="${datas.nameP}"
								readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label>Umur : </label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="number" class="form-control" value="${datas.umurP}"
								readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label>Pekerjaan : </label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" value="${datas.jobP}"
								readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label>Nomor KTP : </label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" value="${datas.ktpP}"
								readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-4">
					<div class="col-xl-2 col-sm-3">
						<label>Alamat : </label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" value="${datas.alamatP}"
								readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-4">
					<div class="col-xl-2 col-sm-3">
						<p>Dalam hal ini untuk dan &nbsp;</p>
					</div>
					<div class="col">
						<div class="btn-group btn-group-sm btn-group-toggle"
							data-toggle="buttons">

							<label class="btn btn-secondary btn-sm"> <input
								type="radio" name="data.kuasa" id="ya" value="ya"
								autocomplete="off" readonly> Atas nama diri sendiri
							</label>

							<p>&nbsp;/&nbsp;</p>

							<label class="btn btn-secondary btm-sm"> <input
								type="radio" name="data.kuasa" id="tidak" value="tidak"
								autocomplete="off" readonly> Selaku kuasa dari
							</label>
						</div>

					</div>
				</div>


				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label>Nama :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" value="${datas.nameK}"
								readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label>Umur :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="number" class="form-control" value="${datas.umurK}"
								readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label>Pekerjaan :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" value="${datas.jobK}"
								readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label>Nomor KTP :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" value="${datas.ktpK}"
								readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-4">
					<div class="col-xl-2 col-sm-3">
						<label>Alamat :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" value="${datas.alamatK}"
								readonly>
						</div>
					</div>
				</div>


				<div class="form-row mb-1">
					<div class="col-xl-auto col-sm-4">
						<label>Berdasarkan Surat Kuasa Nomor</label>
					</div>
					<div class="col-xl-2 col-sm-3">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" value="${datas.noSK}"
								readonly>
						</div>
					</div>
					<div class="col-xl-auto col-sm-3">
						<label>tanggal</label>
					</div>
					<div class="col-xl-2 col-sm-3">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" value="${datas.tglSk}"
								readonly>
						</div>
					</div>
					<div class="col-xl-auto col-sm-4">
						<p>dengan ini mengajukan permohonan :
					</div>
				</div>

				<div class="form-row mb-3"></div>

				<input type="hidden" value="${datas.permohonan}" id="perm" />

				<div class="form-row mb-3">
					<div class="col-xl-5 col-sm-5">
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="1" value="1"
								disabled>Pengukuran
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="2" value="2"
								disabled>Konversi/Pendaftaran
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="3" value="3"
								disabled>Pendaftaran Hak Milik Sarusun
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="4" value="4"
								disabled>Pendaftaran Tanah Wakaf
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="5" value="5"
								disabled>Pendaftaran Peralihan
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="6" value="6"
								disabled>Pendaftaran Pemindahan Hak
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="7" value="7"
								disabled>Pendaftaran Perubahan Hak
							</label>
						</div>
					</div>

					<div class="col-xl-7 col-sm-7">
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="8" value="8"
								disabled>Pemecahan/Penggabungan Hak
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="9" value="9"
								disabled>Pendaftaran Hak Tanggungan
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="10"
								value="10" disabled>Roya Atas Hak Tanggungan
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="11"
								value="11" disabled>Penerbitan Hak Pengganti
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="12"
								value="12" disabled>Surat Keterangan Pendaftaran Tanah
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="13"
								value="13" disabled>Pengecekan Sertifikat
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label"> <input type="radio"
								class="form-check-input" name="data.permohonan" id="14"
								value="14" disabled>Pencatatan <input type="text"
								value="${datas.pencatatan}" readonly>
							</label>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">

					<div class="col-12 mb-3">
						<p>Atas bidang Tanah Hak/Tanah Negara :
					</div>

					<div class="col-xl-2 col-sm-3">
						<label>Terletak di :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control"
								value="${datas.letakTanah}" readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label>Desa/Kelurahan :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" value="${datas.desa}"
								readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label>Kecamatan :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control"
								value="${datas.kecamatan}" readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label>Kabupaten/Kotamadya :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" value="Jakarta Utara"
								readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-2 col-sm-3">
						<label>Nomor Hak :</label>
					</div>
					<div class="col">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" value="${datas.noHak}"
								readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-12 col-sm-12">
						<label>Untuk melengkapi permohonan dimaksud bersama ini
							kami lampirkan :</label>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-12 col-sm-12">

						<div class="input-group input-group-sm">
							<input type="text" class="form-control"
								value="${datas.lampiran1}" readonly>
						</div>

					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-12 col-sm-12">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control"
								value="${datas.lampiran2}" readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-1">
					<div class="col-xl-12 col-sm-12">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control"
								value="${datas.lampiran3}" readonly>
						</div>
					</div>
				</div>

				<div class="form-row mb-5">
					<div class="col-xl-12 col-sm-12">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control"
								value="${datas.lampiran4}" readonly>
						</div>
					</div>
				</div>

			</form>

		</div>
	</div>
	<jsp:include page="../fragments/footer.jsp" />
	<script>
	fill();
	function fill() {
		var checked = document.getElementById('perm').value;
		document.getElementById(checked).checked = true;
	}
	</script>

</body>
</html>