<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<style>
html, body {
	height: 100%;
}

#page-content {
	flex: 1 0 auto;
}
</style>

<footer class="footer bg-dark" style="margin: 0; padding: 40px;">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 my-auto h-100 text-center text-lg-left">
				<ul class="list-inline mb-2">
					<li class="list-inline-item"><a href="#">About</a></li>
					<li class="list-inline-item"><span>-</span></li>
					<li class="list-inline-item"><a href="#">Contact</a></li>
				</ul>
				<p class="text-muted small mb-4 mb-lg-0">&copy; BPN Jakarta
					Utara 2020. All Rights Reserved.</p>
			</div>
			<div class="col-lg-6 my-auto h-100 text-center text-lg-right">
				<ul class="list-inline mb-0">
					<li class="list-inline-item"><a
						href="https://www.facebook.com/KantahJakartaUtara"><i
							class="fa fa-facebook fa-2x fa-fw"></i></a></li>

					<li class="list-inline-item"><a
						href="https://twitter.com/atr_bpnjakut?lang=en"><i
							class="fa fa-twitter fa-2x fa-fw"></i></a></li>

					<li class="list-inline-item"><a
						href="https://www.instagram.com/bpnjakartautara/?hl=en"><i
							class="fa fa-instagram fa-2x fa-fw"></i></a></li>

					<li class="list-inline-item"><a
						href="https://www.youtube.com/channel/UCcw8m4apGvO9SXIK46xzI3A"><i
							class="fa fa-youtube-play fa-2x fa-fw"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>