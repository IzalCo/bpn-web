$(document)
		.ready(
				function() {

					var current_fs, next_fs, previous_fs;

					var steps = $(".card-body").length;
					var current = 1;
					setProgressBar(current);

					$(".next")
							.click(
									function() {

										str1 = "next1";
										str2 = "next2";
										str3 = "next3";

										if ((!str1.localeCompare($(this).attr(
												'id')))
												|| (!str2.localeCompare($(this)
														.attr('id')))
												|| (!str3.localeCompare($(this)
														.attr('id')))) {
											
											current_fs = $(this).parent()
													.parent();
											next_fs = $(this).parent().parent()
													.next();

											$(current_fs).removeClass("show");
											$(next_fs).addClass("show");

											current_fs.animate({}, {
												step : function() {

													current_fs.css({
														'display' : 'none',
														'position' : 'relative'
													});

													next_fs.css({
														'display' : 'block'
													});
												}
											});
											setProgressBar(++current);
											var c = document
													.getElementById('cnt').textContent;
											document.getElementById('cnt').textContent = Number(c) + 25;
										}
									});

					
					$(".prev")
							.click(
									function() {

										current_fs = $(this).parent().parent();
										previous_fs = $(this).parent().parent()
												.prev();

										$(current_fs).removeClass("show");
										$(previous_fs).addClass("show");

										current_fs.animate({}, {
											step : function() {

												current_fs.css({
													'display' : 'none',
													'position' : 'relative'
												});

												previous_fs.css({
													'display' : 'block'
												});
											}
										});
										setProgressBar(--current);
										var c = document.getElementById('cnt').textContent;
										document.getElementById('cnt').textContent = Number(c) - 25;
									});

					function setProgressBar(curStep) {
						var percent = parseFloat(100 / steps) * curStep;
						percent = percent.toFixed();
						$(".progress-bar").css("width", percent + "%");
					}

					$('.radio-group .radio').click(function() {
						$('.selected .fa').removeClass('fa-check');
						$('.selected .fa').addClass('fa-circle');
						$('.radio').removeClass('selected');
						$(this).addClass('selected');
						$('.selected .fa').removeClass('fa-circle');
						$('.selected .fa').addClass('fa-check');
					});

				});