// Disable form submissions if there are invalid fields
(function() {
	'use strict';
	'use strict';
	window.addEventListener('load', function() {
		// Get the forms we want to add validation styles to
		var forms = document.getElementsByClassName('needs-validation');
		// Loop over them and prevent submission
		var validation = Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {
				if (form.checkValidity() === false) {
					event.preventDefault();
					event.stopPropagation();
				}
				form.classList.add('was-validated');
			}, false);
		});
	}, false);
})();

var limit = 4; // Max lampiran
var count = 0;

function addLampiran() {
	// Get the lampiran form element
	var lampiran = document.getElementById('formulir');

	// Good to do error checking, make sure we managed to get something
	if (lampiran) {
		if (count < limit) {
			// Create a new <p> element
			var newP = document.createElement('p');
			newP.innerHTML = 'Question ' + (count + 1);

			// Create the new text box
			var newInput = document.createElement('input');
			newInput.type = 'text';
			newInput.name = 'questions[]';

			// Good practice to do error checking
			if (newInput && newP) {
				// Add the new elements to the form
				lampiran.appendChild(newP);
				lampiran.appendChild(newInput);
				// Increment the count
				count++;
			}

		} else {
			alert('Question limit reached');
		}
	}
}