var lookup = {
   'cilincing': ['Cilincing', 'Kalibaru', 'Marunda', 'Rorotan', 'Semper Barat', 'Semper Timur', 'Sukapura'],
   'kelapa gading': ['Kelapa Gading Barat', 'Kelapa Gading Timur', 'Pegangsaan Dua'],
   'koja': ['Koja', 'Lagoa', 'Rawa Badak Selatan', 'Rawa Badak Utara', 'Tugu Selatan', 'Tugu Utara'],
   'pademangan': ['Ancol', 'Pademangan Barat', 'Pademangan Timur'],
   'penjaringan': ['Kamal Muara', 'Kapuk Muara', 'Pejagalan', 'Penjaringan', 'Pluit'],
   'tanjung priok': ['Kebon Bawang', 'Papanggo', 'Sungai Bambu', 'Sunter Agung', 'Sunter Jaya', 'Tanjung Priok', 'Warakas'],
};

document.getElementById('data.kecamatan').onchange = function() {
    document.getElementById('data.desa').disabled = false; //enabling data.desa select
    document.getElementById('data.desa').innerHTML = ""; //clear data.desa to avoid conflicts between options values
    
    for (i = 0; i < lookup[this.value].length; i++) {
        // Output choice in the target field
    	var opt = document.createElement('option');
    	opt.textContent = lookup[this.value][i]
    	document.getElementById('data.desa').appendChild(opt);
     }
    
};

let element = document.getElementById('data.kecamatan');
let selOption = element.options[element.selectedIndex].value;

if(selOption == 'cilincing'){
    for (i = 0; i < lookup['cilincing'].length; i++) {
        // Output choice in the target field
    	var opt = document.createElement('option');
    	opt.textContent = lookup['cilincing'][i]
    	document.getElementById('data.desa').appendChild(opt);
     }
}