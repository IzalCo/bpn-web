package me.samuel81.pengaduan;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.io.IOUtils;

public class ReportManager {

	private static Date date = new Date();
	private static SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-YYYY");
	private static DateFormat formatter1 = DateFormat.getDateInstance(DateFormat.LONG, new Locale("id", "ID"));
	private static int pos = 1;
	
	private static Map<Integer, ReportDto> reportDtos = new HashMap<>();
	
	private static byte[] pdf;
	
	static {
		formatter.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
		try {
			InputStream input = new URL("https://drive.google.com/uc?export=view&id=1_0PFojS1WyGh9mR0mNpjC6XM1vw1hc8k").openStream();
			pdf = IOUtils.toByteArray(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static InputStream getPDF() {
		return new ByteArrayInputStream(pdf);
	}
	
	public static int getCurrentPos() {
		return pos;
	}
	
	public static String getCurrentDateFull() {
		return formatter1.format(date);
	}
	
	public static String getCurrentDateShort() {
		return formatter.format(date);
	}
	
	public static ReportDto getReport(String id) {
		int i = Integer.valueOf(id);
		return reportDtos.get(i);
	}
	
	public static List<ReportDto> getReports() {
		return new ArrayList<>(reportDtos.values());
	}
	
	public static ReportDto postReport(Map<String, String> maps, String admin) {
		ReportDto reportDto = new ReportDto(pos, maps, admin);
		reportDtos.put(pos, reportDto);
		pos++;
		return reportDto;
	}

}
