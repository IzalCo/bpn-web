package me.samuel81.pengaduan;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

public class ReportDto {
	
    @NotNull
    @NotEmpty
	private int id = 0;
	
    @NotNull
    @NotEmpty
	private String date, fullDate;

    @NotNull
    @NotEmpty
	private String name, alamat, pekerjaan, notelp, pengaduan, admin;

    @NotNull
    @NotEmpty
	private List<String> bukti = new ArrayList<>();

	public ReportDto(int id, Map<String, String> data, String admin) {
		this.name = data.get("nama");
		this.alamat = data.get("alamat");
		this.pekerjaan = data.get("job");
		this.notelp = data.get("hp");
		this.pengaduan = data.get("aduan");

		for (int i = 1; i <= 7; i++) {
			String bk = data.get("bukti." + i);
			if (bk == null) {
				bukti.add("");
				continue;
			}
			bukti.add(data.get("bukti." + i));
		}

		this.id = id;
		this.date = ReportManager.getCurrentDateShort();
		this.fullDate = ReportManager.getCurrentDateFull();
		this.admin = admin;
	}

	public String getName() {
		return name;
	}

	public String getAlamat() {
		return alamat;
	}

	public String getPekerjaan() {
		return pekerjaan;
	}

	public String getNotelp() {
		return notelp;
	}

	public String getPengaduan() {
		return pengaduan;
	}

	public int getId() {
		return id;
	}

	public String getDate() {
		return date;
	}
	
	public String getFullDate() {
		return date;
	}
	
	public String getAdmin() {
		return admin;
	}
	
	public List<String> getBukti() {
		return bukti.stream().filter(k->!k.equalsIgnoreCase("")).collect(Collectors.toList());
	}

	public byte[] printForm() {
		byte[] pdf = null;
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			PDDocument doc = PDDocument.load(ReportManager.getPDF());

			// Page 1
			PDPage page = doc.getPage(0);

			PDPageContentStream contentStream = new PDPageContentStream(doc, page, AppendMode.APPEND, true, true);

			contentStream.beginText();
			contentStream.setFont(PDType1Font.HELVETICA, 10);
			contentStream.setNonStrokingColor(Color.DARK_GRAY);

			contentStream.newLineAtOffset(330f, 824f);
			contentStream.showText(String.valueOf(id));
			contentStream.newLineAtOffset(0f, -15f);
			contentStream.showText(ReportManager.getCurrentDateShort());

			contentStream.newLineAtOffset(-88f, -46.5f);
			contentStream.showText(name);
			contentStream.newLineAtOffset(0f, -15f);
			contentStream.showText(alamat.length() > 60 ? alamat.substring(0, 60):alamat);
			contentStream.newLineAtOffset(0f, -15f);
			contentStream.showText(pekerjaan);
			contentStream.newLineAtOffset(0f, -15f);
			contentStream.showText(notelp);

			contentStream.newLineAtOffset(-160f, -46f);

			contentStream.setFont(PDType1Font.HELVETICA, 9);
			contentStream.setNonStrokingColor(Color.DARK_GRAY);

			String[] str = pengaduan.split("(?<=\\G.{75})");
			for (int i = 0; i < 6; i++) {
				contentStream.showText(i >= str.length ? "" : str[i]);
				contentStream.newLineAtOffset(0, -20.5f - (0.3f * i));
			}

			int i = 0;
			char[] alp = "abcdefghijklmnopqrstuvwxyz".toCharArray();
			contentStream.newLineAtOffset(-5f, -50f);
			for (String buk : bukti) {
				if (buk.length() > 73)
					buk = buk.substring(0, 73);
				contentStream.showText(buk.equalsIgnoreCase("") ? "" : alp[i] + ". " + buk);
				contentStream.newLineAtOffset(0, -20.5f - (0.1f * i++));
			}

			contentStream.setFont(PDType1Font.HELVETICA, 10);
			contentStream.setNonStrokingColor(Color.DARK_GRAY);
			
			contentStream.newLineAtOffset(36f, -99f);
			contentStream.showText(fullDate);

			contentStream.newLineAtOffset(-36f, -91f);
			contentStream.showText(admin);

			contentStream.newLineAtOffset(330f, 0f);
			contentStream.showText(name);
			//

			contentStream.endText();

			contentStream.close();

			doc.save(os);
			doc.close();

			pdf = os.toByteArray();
		} catch (IOException e) { // TODO Auto-generated catch block e.printStackTrace();

		}

		return pdf;
	}

}
