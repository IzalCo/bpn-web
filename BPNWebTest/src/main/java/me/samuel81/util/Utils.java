package me.samuel81.util;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.codec.binary.Base64;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import me.samuel81.form.manager.FormsManager;

public class Utils {

	private static QRCodeWriter qrGen = new QRCodeWriter();

	private static Map<String, byte[]> forms = new HashMap<>();

	private static BufferedImage logo = null;

	public static InputStream getFormFile(String formName) {
		if (forms.containsKey(formName)) {
			byte[] by = forms.get(formName);
			InputStream st = new ByteArrayInputStream(by);
			return st;
		}
		try {
			InputStream input = new URL(FormsManager.getForm(formName).getDriveLink()).openStream();
			byte[] by = IOUtils.toByteArray(input);
			forms.put(formName, by);
			return getFormFile(formName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] generateQRCode(int ukuran, String context) {
		byte[] base64 = null;
		try {
			// InetAddress inetAddress = InetAddress.getLocalHost();
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			BitMatrix bitMatrix = qrGen.encode(/* inetAddress.getHostAddress()+"/form/fetch?formUUID="+ */context,
					BarcodeFormat.QR_CODE, ukuran, ukuran);
			ImageIO.write(MatrixToImageWriter.toBufferedImage(bitMatrix), "PNG", os);
			base64 = os.toByteArray();
		} catch (WriterException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return base64;
	}

	public static String byteToBase64(byte[] b) {
		return Base64.encodeBase64String(b);
	}

	/**
	 * @return string that has been capitalized the first letter
	 */
	private static String capitalize(String input) {
		return input.substring(0, 1).toUpperCase() + input.substring(1).toLowerCase();
	}

	/**
	 * Creating an first letter uppercase
	 * 
	 * @param input
	 * @return
	 */
	private static String lowerFirst(String input) {
		String output = "";
		String[] test = input.contains(" ") ? input.split(" ") : null;
		if (test != null) {
			for (String s : test) {
				output = output == "" ? new StringBuilder().append(capitalize(s)).toString()
						: new StringBuilder().append(output).append(" ").append(capitalize(s)).toString();
			}
		}
		return test != null ? output : capitalize(input);
	}

	/**
	 * 
	 * @param input
	 * @return
	 */
	public static String capitalizer(String input) {
		return lowerFirst(input);
	}

	public static byte[] generateQRWithLogo(String content) {
		byte[] base64 = null;
		// Create new configuration that specifies the error correction
		Map<EncodeHintType, ErrorCorrectionLevel> hints = new HashMap<>();
		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);

		QRCodeWriter writer = new QRCodeWriter();
		BitMatrix bitMatrix = null;
		ByteArrayOutputStream os = new ByteArrayOutputStream();

		try {
			// init directory

			// Create a qr code with the url as content and a size of WxH px
			bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, 400, 400, hints);

			// Load QR image
			BufferedImage qrImage = MatrixToImageWriter.toBufferedImage(bitMatrix);

			// Load logo image
			BufferedImage overly = getLogo();

			// Calculate the delta height and width between QR code and logo
			int deltaHeight = qrImage.getHeight() - overly.getHeight();
			int deltaWidth = qrImage.getWidth() - overly.getWidth();

			// Initialize combined image
			BufferedImage combined = new BufferedImage(qrImage.getHeight(), qrImage.getWidth(),
					BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = (Graphics2D) combined.getGraphics();

			// Write QR code to new image at position 0/0
			g.drawImage(qrImage, 0, 0, null);
			g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));

			// Write logo into combine image at position (deltaWidth / 2) and
			// (deltaHeight / 2). Background: Left/Right and Top/Bottom must be
			// the same space for the logo to be centered
			g.drawImage(overly, (int) Math.round(deltaWidth / 2), (int) Math.round(deltaHeight / 2), null);

			// Write combined image as PNG to OutputStream
			ImageIO.write(combined, "png", os);
			// Store Image
			base64 = os.toByteArray();

		} catch (WriterException e) {
			e.printStackTrace();
			// LOG.error("WriterException occured", e);
		} catch (IOException e) {
			e.printStackTrace();
			// LOG.error("IOException occured", e);
		}
		return base64;
	}

	private static BufferedImage getLogo() throws IOException {
		if (logo != null)
			return logo;
		logo = ImageIO.read(new URL("https://drive.google.com/uc?export=view&id=18YMoHD6NlaNDcBaVnKkP448PY4rPp9eJ"));
		return logo;
	}

}
