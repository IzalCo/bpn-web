package me.samuel81.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import me.samuel81.form.Form;
import me.samuel81.form.manager.FormsManager;
import me.samuel81.util.Utils;

@Controller
public class MainController {

	// private final Logger logger = LoggerFactory.getLogger(MainController.class);

	/**
	 * Home page Return every single registered forms
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/formlist", method = RequestMethod.GET)
	public String lampiran(Model model) {
		model.addAttribute("forms", FormsManager.getForms().keySet());
		return "forms/list";
	}

	/**
	 * Home page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		return "core/index";
	}

	/**
	 * Get form based on @formName Return the PDF file
	 * 
	 * @param formName
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@GetMapping(value = "/form")
	@ResponseBody
	public ResponseEntity<byte[]> getForm(@RequestParam String formName) throws FileNotFoundException, IOException {
		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		
		headers.add("content-disposition", "inline;filename=" + formName + ".pdf");

		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(
				IOUtils.toByteArray(Utils.getFormFile(formName)), headers, HttpStatus.OK);
		return response;
	}

	/**
	 * For filling the form
	 * 
	 * @param formName
	 * @param model
	 * @return
	 */
	@GetMapping(value = "/form/ajukan")
	public String lihatForm(@RequestParam String formName) {
		return "forms/" + formName;
	}

	/**
	 * Recieve data from form and then generate QR Code and PDF based on form data
	 * 
	 * @param formName
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/form/ajukan", method = RequestMethod.POST)
	public String submitForm(@RequestParam String formName, Model model, HttpServletRequest request)
			throws IOException {
		Map<String, String> data = new HashMap<>();
		request.getParameterMap().forEach((k, v) -> {
			if (k.startsWith("data.")) {
				k = k.replace("data.", "");
				data.put(k, v[0]);
			}
		});
		Form form = FormsManager.getForm(formName);
		String uuid = UUID.randomUUID().toString();
		byte[] img = Utils.generateQRWithLogo(uuid);
		byte[] pdf = form.generatePDF(data, img);
		model.addAttribute("formFile", Utils.byteToBase64(pdf));
		model.addAttribute("qrCode", Utils.byteToBase64(img));
		FormsManager.saveForm(uuid, formName, data);
		return "forms/show";
	}

	/**
	 * @RequestMapping(value = "/logo", method = RequestMethod.GET, produces =
	 *                       MediaType.IMAGE_PNG_VALUE) public @ResponseBody byte[]
	 *                       test() throws Exception { return
	 *                       IOUtils.toByteArray(new FileInputStream(new
	 *                       File("/resources/img/logo.png"))); }
	 **/

}