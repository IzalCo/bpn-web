package me.samuel81.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import me.samuel81.form.manager.FormsManager;
import me.samuel81.form.manager.SavedForm;
import me.samuel81.main.PdfUserDetails;
import me.samuel81.main.UserService;
import me.samuel81.main.visitor.CheckBerkas;
import me.samuel81.pengaduan.ReportDto;
import me.samuel81.pengaduan.ReportManager;
import me.samuel81.user.User;
import me.samuel81.util.Utils;

@SessionAttributes({"currentUser"})
@Controller
@RequestMapping("/admin/*")
public class AdminController {

    @Autowired
    private UserService userService;
    
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPost(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");
        
		return "core/login";
	}
	

    @RequestMapping(value = "/postLogin", method = RequestMethod.POST)
    public String postLogin(Model model, HttpSession session) {
        // read principal out of security context and set it to session
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        validatePrinciple(authentication.getPrincipal());
        User loggedInUser = ((PdfUserDetails) authentication.getPrincipal()).getUserDetails();
        model.addAttribute("currentUser", loggedInUser.getUsername());
        session.setAttribute("userId", loggedInUser.getId());
        return "redirect:/";
    }
    private void validatePrinciple(Object principal) {
        if (!(principal instanceof PdfUserDetails)) {
            throw new  IllegalArgumentException("Principal can not be null!");
        }
    }

	@RequestMapping(value = "/users")
	public String users(Model model) {
		model.addAttribute("users", userService.findAll());
		return "core/users";
	}

	@RequestMapping(value = "/pengaduan/tables")
	public String pengaduans(Model model) {
		model.addAttribute("reports", ReportManager.getReports());
		return "pengaduan/table";
	}

	/**
	 * For filling the form
	 * 
	 * @param formName
	 * @param model
	 * @return
	 */
	@GetMapping(value = "/pengaduan")
	public String pengaduan(Authentication authentication, Model model) {
		model.addAttribute("nomor", ReportManager.getCurrentPos());
		model.addAttribute("tanggal", ReportManager.getCurrentDateShort());
		return "pengaduan/pengaduan";
	}

	@RequestMapping(value = "/pengaduan/check")
	public String pengaduan(@RequestParam String id, Model model) {
		ReportDto rep = ReportManager.getReport(id);
		if (rep == null)
			return "error";
		model.addAttribute("dat", rep);

		return "pengaduan/show";
	}

	/**
	 * Recieve data from report form
	 * 
	 * @param formName
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/pengaduan", method = RequestMethod.POST)
	public String submitForm(Authentication authentication, Model model, HttpServletRequest request)
			throws IOException {
		Map<String, String> data = new HashMap<>();
		request.getParameterMap().forEach((k, v) -> {
			if (k.startsWith("data.")) {
				k = k.replace("data.", "");
				data.put(k, v[0]);
			}
		});
		ReportDto rep = ReportManager.postReport(data, userService.findByUsername(authentication.getName()).getFullname());
		model.addAttribute("formFile", Utils.byteToBase64(rep.printForm()));
		return "pengaduan/hasil";
	}

	/**
	 * Open QR reader
	 * 
	 * @return
	 */
	@RequestMapping(value = "/reader")
	public String reader() {
		return "core/reader";
	}

	@RequestMapping(value = "/test")
	public String test(Model model) throws Exception {
		String res = new CheckBerkas().sendPost("5", "2020");
		res = res.substring(res.indexOf("</style>"));
		model.addAttribute("data", res);
		return "core/test";
	}

	/**
	 * Get filled form by form UUID
	 * 
	 * @param formName
	 * @param model
	 * @return
	 */
	@GetMapping(value = "/form/fetch")
	public String fetchForm(@RequestParam String formUUID, Model model) {
		SavedForm sv = FormsManager.getSavedForms(formUUID);
		if (sv == null)
			return "error";
		String formName = sv.getFormName();
		model.addAttribute("uuid", formUUID);
		model.addAttribute("datas", sv.getDatas());
		return "fetch/" + formName;
	}

}