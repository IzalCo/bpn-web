package me.samuel81.main.visitor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.CookieStore;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;

public abstract class WebVisitor {
	
	protected List<String> cookies = new ArrayList<>();
	
	protected CloseableHttpClient httpClient;
	protected CookieStore cookieStore;
	
	protected String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36";
	
	protected String COOKIES_LINK = "";
	protected String LINK = "";

	public WebVisitor() throws Exception {
		cookieStore = new BasicCookieStore();
		SSLContextBuilder builder = new SSLContextBuilder();
		builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
		        builder.build());
		httpClient = HttpClients.custom().setSSLSocketFactory(
		        sslsf).setDefaultCookieStore(cookieStore).build();
	}
	
    protected void close() throws IOException {
        httpClient.close();
    }

	public abstract String sendPost(String i, String ii) throws Exception;

	public abstract String sendGet() throws Exception;

	public List<String> getCookies() {
		return cookies;
	}

	public void setCookies(List<String> cookies) {
		this.cookies = cookies;
	}

}