package me.samuel81.main.visitor;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class CheckBerkas extends WebVisitor {

	private String COOKIES_LINK = "https://www.atrbpn.go.id/Publikasi/Informasi-Berkas";
	private String LINK = "https://www.atrbpn.go.id/DesktopModules/MVC/BpnCekBerkas/Item/GetBerkas";
	
	private String ID = "178bd91f09e54c44bb68bf623379dc13";
	
	private String REQUEST_TOKEN = "";

	public CheckBerkas() throws Exception {

	}

	public String sendPost(String i, String ii) throws Exception {
		sendGet();
		HttpPost conn = new HttpPost(LINK);

		conn.addHeader("Host", "www.atrbpn.go.id");
		conn.addHeader("User-Agent", USER_AGENT);
		conn.addHeader("Accept", "*/*");
		conn.addHeader("Accept-Encoding", "gzip, deflate, br");
		conn.addHeader("Accept-Language", "en-ID");
		conn.addHeader("Cache-Control", "max-age=0");
		for (String cookie : this.cookies) {
			conn.addHeader("Cookie", cookie.split(";", 1)[0]);
		}
		conn.addHeader("Connection", "keep-alive");
		conn.addHeader("Upgrade-Insecure-Requests", "1");
		conn.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		
		conn.addHeader("ModuleId", "121643");
		conn.addHeader("TabId", "41977");
		conn.addHeader("RequestVerificationToken", "application/x-www-form-urlencoded; charset=UTF-8");

		List<NameValuePair> urlParameters = new ArrayList<>();
		urlParameters.add(new BasicNameValuePair("nomor", i));
		urlParameters.add(new BasicNameValuePair("tahun", ii));
		urlParameters.add(new BasicNameValuePair("id", ID));
		UrlEncodedFormEntity params = new UrlEncodedFormEntity(urlParameters, "UTF-8");
		conn.setEntity(params);
		//conn.addHeader("Content-Length", "" + params.getContentLength());

		try (CloseableHttpResponse response = httpClient.execute(conn)) {
			System.out.println("\nSending 'POST' request to URL : " + LINK);
			System.out.println("Post parameters : " + params.toString());
			System.out.println("Response Code : " + response.getStatusLine());

			String result = EntityUtils.toString(response.getEntity());
			return result;
		}
	}

	public String sendGet() throws Exception {

		HttpGet conn = new HttpGet(COOKIES_LINK);

		// act like a browser
		conn.addHeader("Host", "www.atrbpn.go.id");
		conn.addHeader("User-Agent", USER_AGENT);
		conn.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
		conn.addHeader("Accept-Encoding", "gzip, deflate, br");
		conn.addHeader("Accept-Language", "en-US,en;q=0.9,id;q=0.8");
		conn.addHeader("Cache-Control", "max-age=0");
		conn.addHeader("Connection", "Keep-Alive");
		conn.addHeader("DNT", "1");
		conn.addHeader("Referer", "https://www.google.com/");
		conn.addHeader("Sec-Fetch-Mode", "navigate");
		conn.addHeader("Sec-Fetch-Site", "none");
		conn.addHeader("Sec-Fetch-User", "?1");
		conn.addHeader("Upgrade-Insecure-Requests", "1");
		if (cookies != null) {
			for (String cookie : this.cookies) {
				conn.addHeader("Cookie", cookie.split(";", 1)[0]);
			}
		}
		try (CloseableHttpResponse response = httpClient.execute(conn)) {
			System.out.println("\nSending 'GET' request to URL : " + COOKIES_LINK);
			System.out.println("Response Code : " + response.getStatusLine().toString());

			for (Header str : response.getHeaders("Set-Cookie")) {
				cookies.add(str.getValue());
			}
			// read content
			String content = EntityUtils.toString(response.getEntity());

			// find string between: <select name="fromAccountID"> and </select>
			String begin = "<input name=\"__RequestVerificationToken\" type=\"hidden\" value=\"", end = "\" />";
			int beginPos = content.indexOf(begin), endPos = content.indexOf(end, beginPos);
			REQUEST_TOKEN = content.substring(beginPos+begin.length(), endPos);
			return REQUEST_TOKEN;
		}
		
	}

}