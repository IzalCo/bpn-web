package me.samuel81.main;

public interface SecurityService {
	String findLoggedInUsername();

	void autoLogin(String username, String password);
}
