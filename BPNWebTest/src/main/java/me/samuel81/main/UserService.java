package me.samuel81.main;

import java.util.List;

import me.samuel81.user.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
    
    List<User> findAll();
}
