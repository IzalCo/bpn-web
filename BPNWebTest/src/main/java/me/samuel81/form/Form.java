package me.samuel81.form;

import java.util.Map;

public abstract class Form {

	String name;
	String driveLink;

	public abstract byte[] generatePDF(Map<String, String> map, byte[] qrcode);
	
	public String getName() {
		return name;
	}
	
	public String getDriveLink() {
		return driveLink;
	}

}
