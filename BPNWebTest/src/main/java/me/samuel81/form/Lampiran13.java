package me.samuel81.form;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import me.samuel81.util.Utils;

public class Lampiran13 extends Form {

	public Lampiran13() {
		name = "Lampiran 13";
		driveLink = "https://drive.google.com/uc?export=view&id=1Ws31esYQiAtlJF9dcHrNVkKVbze6WW_T";
	}

	public byte[] generatePDF(Map<String, String> map, byte[] qrcode) {
		byte[] pdf = null;
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			PDDocument doc = PDDocument.load(Utils.getFormFile(name));
			PDPage page = doc.getPage(0);

			PDImageXObject pdImage = PDImageXObject.createFromByteArray(doc, qrcode, name);
			PDPageContentStream contentStream = new PDPageContentStream(doc, page, AppendMode.APPEND, true, true);
			//System.out.println("Max x: "+page.getMediaBox().getWidth()+", Max y: "+page.getMediaBox().getHeight());
			
			/*
			 * Drawing QR Code and writing data
			 */
			contentStream.drawImage(pdImage, 64, 800, pdImage.getWidth() / 6, pdImage.getHeight() / 6);
			contentStream.beginText();
			contentStream.setFont(PDType1Font.HELVETICA, 10);
			contentStream.setNonStrokingColor(Color.DARK_GRAY);

			// Tanggal
			Date date = new Date();
			DateFormat formatter = DateFormat.getDateInstance(DateFormat.LONG, new Locale("id", "ID"));
			formatter.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
			contentStream.newLineAtOffset(400, 829);
			contentStream.showText(formatter.format(date));

			boolean sendiri = map.get("kuasa").equalsIgnoreCase("ya");

			/*
			 * Form
			 */
			// Pemilik Tanah
			contentStream.newLineAtOffset(-245, -123);
			contentStream.showText(sendiri ? map.get("nameP") : map.get("nameK"));
			contentStream.newLineAtOffset(0, -14);
			contentStream.showText(sendiri ? map.get("umurP") : map.get("umurK"));
			contentStream.newLineAtOffset(0, -14);
			contentStream.showText(sendiri ? map.get("jobP") : map.get("jobK"));
			contentStream.newLineAtOffset(0, -13);
			contentStream.showText(sendiri ? map.get("ktpP") : map.get("ktpK"));
			contentStream.newLineAtOffset(0, -13.2f);
			contentStream.showText(sendiri ? map.get("alamatP") : map.get("alamatK"));

			// Kuasa
			contentStream.newLineAtOffset(0, -54);
			contentStream.showText(!sendiri ? map.get("nameP") : "");
			contentStream.newLineAtOffset(0, -14);
			contentStream.showText(!sendiri ? map.get("umurP") : "");
			contentStream.newLineAtOffset(0, -14);
			contentStream.showText(!sendiri ? map.get("jobP") : "");
			contentStream.newLineAtOffset(0, -13);
			contentStream.showText(!sendiri ? map.get("ktpP") : "");
			contentStream.newLineAtOffset(0, -13.2f);
			contentStream.showText(!sendiri ? map.get("alamatP") : "");

			// Nomor SK
			contentStream.newLineAtOffset(100, -26);
			contentStream.showText(map.get("noSK"));
			contentStream.newLineAtOffset(108, 0);
			contentStream.showText(map.get("tglSk"));

			contentStream.setFont(PDType1Font.SYMBOL, 25);
			boolean lainnya = false;
			switch (Integer.valueOf(map.get("permohonan"))) {
			case 1:
				contentStream.newLineAtOffset(-293, -47);
				contentStream.showText("•");
				contentStream.newLineAtOffset(95, -131);
				break;
			case 2:
				contentStream.newLineAtOffset(-293, -60); // 13
				contentStream.showText("•");
				contentStream.newLineAtOffset(95, -118);
				break;
			case 3:
				contentStream.newLineAtOffset(-293, -74); // 14
				contentStream.showText("•");
				contentStream.newLineAtOffset(95, -104);
				break;
			case 4:
				contentStream.newLineAtOffset(-293, -87); // 13
				contentStream.showText("•");
				contentStream.newLineAtOffset(95, -91);
				break;
			case 5:
				contentStream.newLineAtOffset(-293, -101); // 14
				contentStream.showText("•");
				contentStream.newLineAtOffset(95, -77);
				break;
			case 6:
				contentStream.newLineAtOffset(-293, -115); // 14
				contentStream.showText("•");
				contentStream.newLineAtOffset(95, -63);
				break;
			case 7:
				contentStream.newLineAtOffset(-293, -128); // 13
				contentStream.showText("•");
				contentStream.newLineAtOffset(95, -50);
				break;

			// next line
			case 8:
				contentStream.newLineAtOffset(-76, -47);
				contentStream.showText("•");
				contentStream.newLineAtOffset(-122, -131);
				break;
			case 9:
				contentStream.newLineAtOffset(-76, -60);
				contentStream.showText("•");
				contentStream.newLineAtOffset(-122, -118);
				break;
			case 10:
				contentStream.newLineAtOffset(-78, -74);
				contentStream.showText("•");
				contentStream.newLineAtOffset(-122, -104);
				break;
			case 11:
				contentStream.newLineAtOffset(-78, -87);
				contentStream.showText("•");
				contentStream.newLineAtOffset(-122, -91);
				break;
			case 12:
				contentStream.newLineAtOffset(-78, -101);
				contentStream.showText("•");
				contentStream.newLineAtOffset(-122, -77);
				break;
			case 13:
				contentStream.newLineAtOffset(-78, -115);
				contentStream.showText("•");
				contentStream.newLineAtOffset(-122, -63);
				break;
			case 14:
				contentStream.newLineAtOffset(-78, -128);
				contentStream.showText("•");
				lainnya = true;
				break;
			}
			contentStream.setFont(PDType1Font.HELVETICA, 10);
			contentStream.setNonStrokingColor(Color.DARK_GRAY);
			if (lainnya) {
				contentStream.newLineAtOffset(90, 5);
				contentStream.showText(map.get("pencatatan"));
				contentStream.newLineAtOffset(-212, -53);
			}

			// Data letak tanah
			contentStream.showText(map.get("letakTanah"));
			contentStream.newLineAtOffset(-1, -14);
			contentStream.showText(Utils.capitalizer(map.get("desa")));
			contentStream.newLineAtOffset(0, -15);
			contentStream.showText(Utils.capitalizer(map.get("kecamatan")));
			contentStream.newLineAtOffset(0, -30);
			contentStream.showText(map.get("noHak"));

			// Lampiran
			contentStream.newLineAtOffset(-80, -30);
			contentStream.showText(map.get("lampiran1"));
			contentStream.newLineAtOffset(0, -15.5f);
			contentStream.showText(map.get("lampiran2"));
			contentStream.newLineAtOffset(0, -14.5f);
			contentStream.showText(map.get("lampiran3"));
			contentStream.newLineAtOffset(0, -16);
			contentStream.showText(map.get("lampiran4"));

			contentStream.endText();

			if (sendiri) {
				contentStream.moveTo(298, 626);
				contentStream.lineTo(380, 626);
			} else {
				contentStream.moveTo(188, 626);
				contentStream.lineTo(292, 626);
			}
			contentStream.stroke();

			contentStream.close();

			doc.save(os);
			doc.close();

			pdf = os.toByteArray();
		} catch (IOException e) { // TODO Auto-generated catch block e.printStackTrace();

		}

		return pdf;
	}

	public String getPermohonan(int type) {
		switch (type) {
		case 1:
			return "Pengukuran";
		case 2:
			return "Konversi/Pendaftaran";
		case 3:
			return "Pendaftaran Hak Milik Sarusun";
		case 4:
			return "Pendaftaran Tanah Wakaf";
		case 5:
			return "Pendaftaran Peralihan Hak";
		case 6:
			return "Pendaftaran Pemindahan Hak";
		case 7:
			return "Pendaftaran Perubahan Hak";
		case 8:
			return "Pemecahan/Penggabungan Hak";
		case 9:
			return "Pendaftaran Hak Tanggungan";
		case 10:
			return "Roya Atas Hak Tanggungan";
		case 11:
			return "Penerbitan Hak Pengganti";
		case 12:
			return "Surat Keterangan Pendaftaran Tanah";
		case 13:
			return "Pengecekan Sertifikat";
		case 14:
			return "Pencatatan";
		default:
			return "";
		}
	}

	public String toString() {
		return "";
	}

}
