package me.samuel81.form.manager;

import java.util.HashMap;
import java.util.Map;

import me.samuel81.form.Form;
import me.samuel81.form.FormPenurunanHak;
import me.samuel81.form.Lampiran13;

public class FormsManager {

	private static Map<String, Form> forms = new HashMap<>();
	private static Map<String, SavedForm> savedForms = new HashMap<>();

	static {
		forms.put("Lampiran 13", new Lampiran13());
		forms.put("Formulir Penurunan Hak", new FormPenurunanHak());
	}

	public static Map<String, Form> getForms() {
		return forms;
	}
	
	public static Map<String, SavedForm> getSavedForms() {
		return savedForms;
	}
	
	public static SavedForm getSavedForms(String uuid) {
		return savedForms.get(uuid);
	}

	public static Form getForm(String name) {
		return forms.get(name);
	}
	
	public static void saveForm(String uuid, String formName, Map<String, String> datas) {
		SavedForm sv =  new SavedForm(uuid, formName, datas);
		savedForms.put(uuid, sv);
	}

}
