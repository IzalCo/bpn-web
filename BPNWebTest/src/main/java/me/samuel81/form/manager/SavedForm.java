package me.samuel81.form.manager;

import java.util.Map;

public class SavedForm {
	
	private String uuid;
	private String formName;
	private Map<String, String> datas;
	
	public SavedForm(String uuid, String formName, Map<String, String> data) {
		this.uuid = uuid;
		this.formName = formName;
		this.datas = data;
	}
	
	public String getUniqueId() {
		return uuid;
	}
	
	public Map<String, String> getDatas() {
		return datas;
	}
	
	public String getFormName() {
		return formName;
	}

}
