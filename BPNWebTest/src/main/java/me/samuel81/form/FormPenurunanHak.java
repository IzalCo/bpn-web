package me.samuel81.form;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import me.samuel81.util.Utils;

public class FormPenurunanHak extends Form {

	public FormPenurunanHak() {
		name = "Formulir Penurunan Hak";
		driveLink = "https://drive.google.com/uc?export=view&id=1OclLNVZeZYOz_v1JPdhPBAVylunDIGDu";
	}

	public byte[] generatePDF(Map<String, String> map, byte[] qrcode) {
		byte[] pdf = null;
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			PDDocument doc = PDDocument.load(Utils.getFormFile(name));
			
			//Page 1
			PDPage page = doc.getPage(0);

			PDImageXObject pdImage = PDImageXObject.createFromByteArray(doc, qrcode, name);
			PDPageContentStream contentStream = new PDPageContentStream(doc, page, AppendMode.APPEND, true, true);
			//System.out.println("Max x: "+page.getMediaBox().getWidth()+", Max y: "+page.getMediaBox().getHeight());

			/*
			 * Drawing QR Code and writing data
			 */
			contentStream.drawImage(pdImage, 56, 800, pdImage.getWidth() / 6, pdImage.getHeight() / 6);
			contentStream.beginText();
			contentStream.setFont(PDType1Font.HELVETICA, 10);
			contentStream.setNonStrokingColor(Color.DARK_GRAY);

			// Tanggal
			Date date = new Date();
			DateFormat formatter = DateFormat.getDateInstance(DateFormat.LONG, new Locale("id", "ID"));
			formatter.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));

			boolean sendiri = map.get("kuasa").equalsIgnoreCase("ya");
			/*
			 * Form
			 */
			// Pemilik Tanah
			contentStream.newLineAtOffset(228f, 720f);
			contentStream.showText(sendiri ? map.get("nameP") : map.get("nameK"));
			contentStream.newLineAtOffset(0, -15f);
			contentStream.showText(sendiri ? map.get("birthP") : map.get("birthK"));
			contentStream.newLineAtOffset(0, -15f);
			contentStream.showText(sendiri ? map.get("jobP") : map.get("jobK"));
			contentStream.newLineAtOffset(0, -15f);
			contentStream.showText(sendiri ? map.get("kewP") : map.get("kewK"));
			contentStream.newLineAtOffset(0, -14.3f);
			contentStream.showText(sendiri ? map.get("alamatP") : map.get("alamatK"));
			
			contentStream.newLineAtOffset(0, -54.5f);
			contentStream.showText(!sendiri ? map.get("nameP") : "");
			contentStream.newLineAtOffset(0, -15f);
			contentStream.showText(!sendiri ? map.get("birthP") : "");
			contentStream.newLineAtOffset(0, -14.5f);
			contentStream.showText(!sendiri ? map.get("jobP") : "");
			contentStream.newLineAtOffset(0, -15f);
			contentStream.showText(!sendiri ? map.get("kewP") : "");
			contentStream.newLineAtOffset(0, -14.3f);
			contentStream.showText(!sendiri ? map.get("alamatP") : "");
			
			contentStream.newLineAtOffset(10, -26);
			contentStream.showText(map.get("noSK"));
			contentStream.newLineAtOffset(175, 0);
			contentStream.showText(map.get("tglSk"));
			
			contentStream.newLineAtOffset(-185, -57);
			contentStream.showText(Utils.capitalizer(map.get("desa")));
			contentStream.newLineAtOffset(0, -15);
			contentStream.showText(Utils.capitalizer(map.get("kecamatan")));
			
			contentStream.newLineAtOffset(75, -130);
			contentStream.showText(map.get("noSerti"));
			contentStream.newLineAtOffset(100, 0);
			contentStream.showText(map.get("tahunSerti"));
			
			contentStream.newLineAtOffset(-160, -13);
			contentStream.showText(map.get("tanggalLelang"));
			contentStream.newLineAtOffset(110, 0);
			contentStream.showText(map.get("noLelang"));
			
			contentStream.newLineAtOffset(75, -15);
			contentStream.showText(map.get("bank"));
			contentStream.newLineAtOffset(-230, -14);
			contentStream.showText(map.get("bankTgl"));
			contentStream.newLineAtOffset(100, 0);
			contentStream.showText(map.get("bankTgl"));
			
			contentStream.newLineAtOffset(-55, -14);
			contentStream.showText(map.get("ident"));
			
			//
			
			contentStream.newLineAtOffset(190, -72);
			contentStream.showText(formatter.format(date));

			contentStream.endText();

			if (sendiri) {
				contentStream.moveTo(329, 634);
				contentStream.lineTo(415, 634);
			} else {
				contentStream.moveTo(175, 634);
				contentStream.lineTo(328, 634);
			}
			contentStream.stroke();
			contentStream.close();
			
			PDPage page1 = doc.getPage(1);

			contentStream = new PDPageContentStream(doc, page1, AppendMode.APPEND, true, true);
			contentStream.setFont(PDType1Font.HELVETICA, 10);
			contentStream.setNonStrokingColor(Color.DARK_GRAY);
			contentStream.beginText();
			/*
			 * Form
			 */
			// Pemilik Tanah
			contentStream.newLineAtOffset(175f, 800f);
			contentStream.showText(map.get("nameP"));
			contentStream.newLineAtOffset(0, -19.5f);
			contentStream.showText(map.get("umur"));
			contentStream.newLineAtOffset(0, -19.5f);
			contentStream.showText(map.get("pekerjaan"));
			contentStream.newLineAtOffset(0, -19.5f);
			contentStream.showText(map.get("ktpP"));
			contentStream.newLineAtOffset(0, -19.5f);
			contentStream.showText(map.get("alamat"));
			
			contentStream.newLineAtOffset(35, -102f);
			contentStream.showText(map.get("jalanT"));
			contentStream.newLineAtOffset(0, -19.5f);
			contentStream.showText(map.get("desa"));
			contentStream.newLineAtOffset(0, -19.5f);
			contentStream.showText(map.get("luasT"));
			contentStream.newLineAtOffset(0, -19.7f);
			contentStream.showText(map.get("statusT"));
			contentStream.newLineAtOffset(140, 0f);
			contentStream.showText(map.get("noT"));
			
			contentStream.newLineAtOffset(90, -375f);
			contentStream.showText(formatter.format(date));
			
			
			contentStream.endText();
			contentStream.close();

			doc.save(os);
			doc.close();

			pdf = os.toByteArray();
		} catch (IOException e) { // TODO Auto-generated catch block e.printStackTrace();

		}

		return pdf;
	}

	public String toString() {
		return "";
	}

}
