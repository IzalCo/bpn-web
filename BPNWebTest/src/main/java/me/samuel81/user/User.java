package me.samuel81.user;

 import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "users")
public class User {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	
    @NotEmpty
    @Column(nullable = false, unique = true)
	String username;
    @NotEmpty
    String password, fullname, team;
    
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "role_id") })
    private Set<Role> roles = new HashSet<>();
    
    public User() {
    	
    }
	
	public User(String fullName, String username, String password, String team) {
		this.fullname = fullName;
		this.username = username;
		this.password = password;
		this.team = team;
	}
	
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
	
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
	
	public String getFullname() {
		return fullname;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String getTeam() {
		return team;
	}
	
	public void setFullname(String s) {
		this.fullname = s;
	}
	
	public void setUsername(String s) {
		this.username = s;
	}
	
	public void setPassword(String s) {
		this.password = s;
	}
	
	public void setTeam(String s) {
		this.team = s;
	}

}
